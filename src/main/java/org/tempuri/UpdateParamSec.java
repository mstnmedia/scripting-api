
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_exp" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_ses" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_sus" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_int" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_avi" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_car1" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_car2" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_log" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_usu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vExp",
    "vSes",
    "vSus",
    "vInt",
    "vAvi",
    "vCar1",
    "vCar2",
    "vLog",
    "vUsu",
    "vIp"
})
@XmlRootElement(name = "UpdateParamSec")
public class UpdateParamSec {

	/**
	 *
	 */
	@XmlElement(name = "v_exp")
    protected int vExp;

	/**
	 *
	 */
	@XmlElement(name = "v_ses")
    protected int vSes;

	/**
	 *
	 */
	@XmlElement(name = "v_sus")
    protected int vSus;

	/**
	 *
	 */
	@XmlElement(name = "v_int")
    protected int vInt;

	/**
	 *
	 */
	@XmlElement(name = "v_avi")
    protected int vAvi;

	/**
	 *
	 */
	@XmlElement(name = "v_car1")
    protected int vCar1;

	/**
	 *
	 */
	@XmlElement(name = "v_car2")
    protected int vCar2;

	/**
	 *
	 */
	@XmlElement(name = "v_log")
    protected int vLog;

	/**
	 *
	 */
	@XmlElement(name = "v_usu")
    protected String vUsu;

	/**
	 *
	 */
	@XmlElement(name = "v_ip")
    protected String vIp;

    /**
     * Gets the value of the vExp property.
     * 
	 * @return 
     */
    public int getVExp() {
        return vExp;
    }

    /**
     * Sets the value of the vExp property.
     * 
	 * @param value
     */
    public void setVExp(int value) {
        this.vExp = value;
    }

    /**
     * Gets the value of the vSes property.
     * 
	 * @return 
     */
    public int getVSes() {
        return vSes;
    }

    /**
     * Sets the value of the vSes property.
     * 
	 * @param value
     */
    public void setVSes(int value) {
        this.vSes = value;
    }

    /**
     * Gets the value of the vSus property.
     * 
	 * @return 
     */
    public int getVSus() {
        return vSus;
    }

    /**
     * Sets the value of the vSus property.
     * 
	 * @param value
     */
    public void setVSus(int value) {
        this.vSus = value;
    }

    /**
     * Gets the value of the vInt property.
     * 
	 * @return 
     */
    public int getVInt() {
        return vInt;
    }

    /**
     * Sets the value of the vInt property.
     * 
	 * @param value
     */
    public void setVInt(int value) {
        this.vInt = value;
    }

    /**
     * Gets the value of the vAvi property.
     * 
	 * @return 
     */
    public int getVAvi() {
        return vAvi;
    }

    /**
     * Sets the value of the vAvi property.
     * 
	 * @param value
     */
    public void setVAvi(int value) {
        this.vAvi = value;
    }

    /**
     * Gets the value of the vCar1 property.
     * 
	 * @return 
     */
    public int getVCar1() {
        return vCar1;
    }

    /**
     * Sets the value of the vCar1 property.
     * 
	 * @param value
     */
    public void setVCar1(int value) {
        this.vCar1 = value;
    }

    /**
     * Gets the value of the vCar2 property.
     * 
	 * @return 
     */
    public int getVCar2() {
        return vCar2;
    }

    /**
     * Sets the value of the vCar2 property.
     * 
	 * @param value
     */
    public void setVCar2(int value) {
        this.vCar2 = value;
    }

    /**
     * Gets the value of the vLog property.
     * 
	 * @return 
     */
    public int getVLog() {
        return vLog;
    }

    /**
     * Sets the value of the vLog property.
     * 
	 * @param value
     */
    public void setVLog(int value) {
        this.vLog = value;
    }

    /**
     * Gets the value of the vUsu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsu() {
        return vUsu;
    }

    /**
     * Sets the value of the vUsu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsu(String value) {
        this.vUsu = value;
    }

    /**
     * Gets the value of the vIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIp() {
        return vIp;
    }

    /**
     * Sets the value of the vIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIp(String value) {
        this.vIp = value;
    }

}
