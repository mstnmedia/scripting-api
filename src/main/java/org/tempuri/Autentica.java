
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_Usr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_App" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_pass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vUsr",
    "vApp",
    "vPass"
})
@XmlRootElement(name = "Autentica")
public class Autentica {

	/**
	 *
	 */
	@XmlElement(name = "v_Usr")
    protected String vUsr;

	/**
	 *
	 */
	@XmlElement(name = "v_App")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_pass")
    protected String vPass;

    /**
     * Gets the value of the vUsr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsr() {
        return vUsr;
    }

    /**
     * Sets the value of the vUsr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsr(String value) {
        this.vUsr = value;
    }

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vPass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVPass() {
        return vPass;
    }

    /**
     * Sets the value of the vPass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVPass(String value) {
        this.vPass = value;
    }

}
