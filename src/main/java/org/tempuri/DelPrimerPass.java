
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_usu" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vUsu"
})
@XmlRootElement(name = "DelPrimerPass")
public class DelPrimerPass {

	/**
	 *
	 */
	@XmlElement(name = "v_usu")
    protected int vUsu;

    /**
     * Gets the value of the vUsu property.
     * 
	 * @return 
     */
    public int getVUsu() {
        return vUsu;
    }

    /**
     * Sets the value of the vUsu property.
     * 
	 * @param value
     */
    public void setVUsu(int value) {
        this.vUsu = value;
    }

}
