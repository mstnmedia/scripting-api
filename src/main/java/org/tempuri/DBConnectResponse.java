
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DBConnectResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dbConnectResult"
})
@XmlRootElement(name = "DBConnectResponse")
public class DBConnectResponse {

	/**
	 *
	 */
	@XmlElement(name = "DBConnectResult")
    protected boolean dbConnectResult;

    /**
     * Gets the value of the dbConnectResult property.
     * 
	 * @return 
     */
    public boolean isDBConnectResult() {
        return dbConnectResult;
    }

    /**
     * Sets the value of the dbConnectResult property.
     * 
	 * @param value
     */
    public void setDBConnectResult(boolean value) {
        this.dbConnectResult = value;
    }

}
