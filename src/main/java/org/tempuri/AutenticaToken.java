
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_usr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_app" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_token" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vUsr",
    "vApp",
    "vToken"
})
@XmlRootElement(name = "AutenticaToken")
public class AutenticaToken {

	/**
	 *
	 */
	@XmlElement(name = "v_usr")
    protected String vUsr;

	/**
	 *
	 */
	@XmlElement(name = "v_app")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_token")
    protected long vToken;

    /**
     * Gets the value of the vUsr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsr() {
        return vUsr;
    }

    /**
     * Sets the value of the vUsr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsr(String value) {
        this.vUsr = value;
    }

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vToken property.
     * 
	 * @return 
     */
    public long getVToken() {
        return vToken;
    }

    /**
     * Sets the value of the vToken property.
     * 
	 * @param value
     */
    public void setVToken(long value) {
        this.vToken = value;
    }

}
