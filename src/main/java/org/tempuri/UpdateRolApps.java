
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_App" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_rol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_nom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_st" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vApp",
    "vRol",
    "vNom",
    "vSt",
    "vUsuario",
    "vIp"
})
@XmlRootElement(name = "UpdateRolApps")
public class UpdateRolApps {

	/**
	 *
	 */
	@XmlElement(name = "v_App")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_rol")
    protected String vRol;

	/**
	 *
	 */
	@XmlElement(name = "v_nom")
    protected String vNom;

	/**
	 *
	 */
	@XmlElement(name = "v_st")
    protected String vSt;

	/**
	 *
	 */
	@XmlElement(name = "v_usuario")
    protected String vUsuario;

	/**
	 *
	 */
	@XmlElement(name = "v_ip")
    protected String vIp;

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vRol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVRol() {
        return vRol;
    }

    /**
     * Sets the value of the vRol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVRol(String value) {
        this.vRol = value;
    }

    /**
     * Gets the value of the vNom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVNom() {
        return vNom;
    }

    /**
     * Sets the value of the vNom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVNom(String value) {
        this.vNom = value;
    }

    /**
     * Gets the value of the vSt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVSt() {
        return vSt;
    }

    /**
     * Sets the value of the vSt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVSt(String value) {
        this.vSt = value;
    }

    /**
     * Gets the value of the vUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsuario() {
        return vUsuario;
    }

    /**
     * Sets the value of the vUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsuario(String value) {
        this.vUsuario = value;
    }

    /**
     * Gets the value of the vIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIp() {
        return vIp;
    }

    /**
     * Sets the value of the vIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIp(String value) {
        this.vIp = value;
    }

}
