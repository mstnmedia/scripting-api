
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateCuentaResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateCuentaResult"
})
@XmlRootElement(name = "UpdateCuentaResponse")
public class UpdateCuentaResponse {

	/**
	 *
	 */
	@XmlElement(name = "UpdateCuentaResult")
    protected boolean updateCuentaResult;

    /**
     * Gets the value of the updateCuentaResult property.
     * 
	 * @return 
     */
    public boolean isUpdateCuentaResult() {
        return updateCuentaResult;
    }

    /**
     * Sets the value of the updateCuentaResult property.
     * 
	 * @param value
     */
    public void setUpdateCuentaResult(boolean value) {
        this.updateCuentaResult = value;
    }

}
