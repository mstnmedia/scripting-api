
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_app" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_link" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_image" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vApp",
    "vNombre",
    "vLink",
    "vImage",
    "vDesc",
    "vUsuario",
    "vIp"
})
@XmlRootElement(name = "UpdateApps")
public class UpdateApps {

	/**
	 *
	 */
	@XmlElement(name = "v_app")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_nombre")
    protected String vNombre;

	/**
	 *
	 */
	@XmlElement(name = "v_link")
    protected String vLink;

	/**
	 *
	 */
	@XmlElement(name = "v_image")
    protected String vImage;

	/**
	 *
	 */
	@XmlElement(name = "v_desc")
    protected String vDesc;

	/**
	 *
	 */
	@XmlElement(name = "v_usuario")
    protected String vUsuario;

	/**
	 *
	 */
	@XmlElement(name = "v_ip")
    protected String vIp;

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVNombre() {
        return vNombre;
    }

    /**
     * Sets the value of the vNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVNombre(String value) {
        this.vNombre = value;
    }

    /**
     * Gets the value of the vLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVLink() {
        return vLink;
    }

    /**
     * Sets the value of the vLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVLink(String value) {
        this.vLink = value;
    }

    /**
     * Gets the value of the vImage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVImage() {
        return vImage;
    }

    /**
     * Sets the value of the vImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVImage(String value) {
        this.vImage = value;
    }

    /**
     * Gets the value of the vDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVDesc() {
        return vDesc;
    }

    /**
     * Sets the value of the vDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVDesc(String value) {
        this.vDesc = value;
    }

    /**
     * Gets the value of the vUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsuario() {
        return vUsuario;
    }

    /**
     * Sets the value of the vUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsuario(String value) {
        this.vUsuario = value;
    }

    /**
     * Gets the value of the vIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIp() {
        return vIp;
    }

    /**
     * Sets the value of the vIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIp(String value) {
        this.vIp = value;
    }

}
