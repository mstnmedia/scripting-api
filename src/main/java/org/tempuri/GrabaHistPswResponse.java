
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GrabaHistPswResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "grabaHistPswResult"
})
@XmlRootElement(name = "GrabaHistPswResponse")
public class GrabaHistPswResponse {

	/**
	 *
	 */
	@XmlElement(name = "GrabaHistPswResult")
    protected boolean grabaHistPswResult;

    /**
     * Gets the value of the grabaHistPswResult property.
     * 
	 * @return 
     */
    public boolean isGrabaHistPswResult() {
        return grabaHistPswResult;
    }

    /**
     * Sets the value of the grabaHistPswResult property.
     * 
	 * @param value
     */
    public void setGrabaHistPswResult(boolean value) {
        this.grabaHistPswResult = value;
    }

}
