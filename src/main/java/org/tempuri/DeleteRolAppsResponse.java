
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DeleteRolAppsResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deleteRolAppsResult"
})
@XmlRootElement(name = "DeleteRolAppsResponse")
public class DeleteRolAppsResponse {

	/**
	 *
	 */
	@XmlElement(name = "DeleteRolAppsResult")
    protected boolean deleteRolAppsResult;

    /**
     * Gets the value of the deleteRolAppsResult property.
     * 
	 * @return 
     */
    public boolean isDeleteRolAppsResult() {
        return deleteRolAppsResult;
    }

    /**
     * Sets the value of the deleteRolAppsResult property.
     * 
	 * @param value
     */
    public void setDeleteRolAppsResult(boolean value) {
        this.deleteRolAppsResult = value;
    }

}
