
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_app" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_rol" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="v_opc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vApp",
    "vRol",
    "vOpc"
})
@XmlRootElement(name = "AccesoOpcion")
public class AccesoOpcion {

	/**
	 *
	 */
	@XmlElement(name = "v_app")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_rol")
    protected int vRol;

	/**
	 *
	 */
	@XmlElement(name = "v_opc")
    protected String vOpc;

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vRol property.
     * 
	 * @return 
     */
    public int getVRol() {
        return vRol;
    }

    /**
     * Sets the value of the vRol property.
     * 
	 * @param value
     */
    public void setVRol(int value) {
        this.vRol = value;
    }

    /**
     * Gets the value of the vOpc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVOpc() {
        return vOpc;
    }

    /**
     * Sets the value of the vOpc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVOpc(String value) {
        this.vOpc = value;
    }

}
