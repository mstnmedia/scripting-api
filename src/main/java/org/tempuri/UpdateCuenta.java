
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_usu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_celular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vUsu",
    "vCelular",
    "vEmail",
    "vUsuario",
    "vIp"
})
@XmlRootElement(name = "UpdateCuenta")
public class UpdateCuenta {

	/**
	 *
	 */
	@XmlElement(name = "v_usu")
    protected String vUsu;

	/**
	 *
	 */
	@XmlElement(name = "v_celular")
    protected String vCelular;

	/**
	 *
	 */
	@XmlElement(name = "v_email")
    protected String vEmail;

	/**
	 *
	 */
	@XmlElement(name = "v_usuario")
    protected String vUsuario;

	/**
	 *
	 */
	@XmlElement(name = "v_ip")
    protected String vIp;

    /**
     * Gets the value of the vUsu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsu() {
        return vUsu;
    }

    /**
     * Sets the value of the vUsu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsu(String value) {
        this.vUsu = value;
    }

    /**
     * Gets the value of the vCelular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVCelular() {
        return vCelular;
    }

    /**
     * Sets the value of the vCelular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVCelular(String value) {
        this.vCelular = value;
    }

    /**
     * Gets the value of the vEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVEmail() {
        return vEmail;
    }

    /**
     * Sets the value of the vEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVEmail(String value) {
        this.vEmail = value;
    }

    /**
     * Gets the value of the vUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsuario() {
        return vUsuario;
    }

    /**
     * Sets the value of the vUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsuario(String value) {
        this.vUsuario = value;
    }

    /**
     * Gets the value of the vIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIp() {
        return vIp;
    }

    /**
     * Sets the value of the vIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIp(String value) {
        this.vIp = value;
    }

}
