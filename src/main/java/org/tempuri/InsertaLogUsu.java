
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_usu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_msg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_app" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_IP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vUsu",
    "vMsg",
    "vApp",
    "vip"
})
@XmlRootElement(name = "InsertaLogUsu")
public class InsertaLogUsu {

	/**
	 *
	 */
	@XmlElement(name = "v_usu")
    protected String vUsu;

	/**
	 *
	 */
	@XmlElement(name = "v_msg")
    protected String vMsg;

	/**
	 *
	 */
	@XmlElement(name = "v_app")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_IP")
    protected String vip;

    /**
     * Gets the value of the vUsu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsu() {
        return vUsu;
    }

    /**
     * Sets the value of the vUsu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsu(String value) {
        this.vUsu = value;
    }

    /**
     * Gets the value of the vMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVMsg() {
        return vMsg;
    }

    /**
     * Sets the value of the vMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVMsg(String value) {
        this.vMsg = value;
    }

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIP() {
        return vip;
    }

    /**
     * Sets the value of the vip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIP(String value) {
        this.vip = value;
    }

}
