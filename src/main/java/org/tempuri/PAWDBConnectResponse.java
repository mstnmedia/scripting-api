
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PAWDBConnectResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pawdbConnectResult"
})
@XmlRootElement(name = "PAWDBConnectResponse")
public class PAWDBConnectResponse {

	/**
	 *
	 */
	@XmlElement(name = "PAWDBConnectResult")
    protected boolean pawdbConnectResult;

    /**
     * Gets the value of the pawdbConnectResult property.
     * 
	 * @return 
     */
    public boolean isPAWDBConnectResult() {
        return pawdbConnectResult;
    }

    /**
     * Sets the value of the pawdbConnectResult property.
     * 
	 * @param value
     */
    public void setPAWDBConnectResult(boolean value) {
        this.pawdbConnectResult = value;
    }

}
