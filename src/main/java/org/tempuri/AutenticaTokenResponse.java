
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AutenticaTokenResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "autenticaTokenResult"
})
@XmlRootElement(name = "AutenticaTokenResponse")
public class AutenticaTokenResponse {

	/**
	 *
	 */
	@XmlElement(name = "AutenticaTokenResult")
    protected boolean autenticaTokenResult;

    /**
     * Gets the value of the autenticaTokenResult property.
     * 
	 * @return 
     */
    public boolean isAutenticaTokenResult() {
        return autenticaTokenResult;
    }

    /**
     * Sets the value of the autenticaTokenResult property.
     * 
	 * @param value
     */
    public void setAutenticaTokenResult(boolean value) {
        this.autenticaTokenResult = value;
    }

}
