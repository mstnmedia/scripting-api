
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetUsuarioPorTarjetaResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUsuarioPorTarjetaResult"
})
@XmlRootElement(name = "GetUsuarioPorTarjetaResponse")
public class GetUsuarioPorTarjetaResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetUsuarioPorTarjetaResult")
    protected String getUsuarioPorTarjetaResult;

    /**
     * Gets the value of the getUsuarioPorTarjetaResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetUsuarioPorTarjetaResult() {
        return getUsuarioPorTarjetaResult;
    }

    /**
     * Sets the value of the getUsuarioPorTarjetaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetUsuarioPorTarjetaResult(String value) {
        this.getUsuarioPorTarjetaResult = value;
    }

}
