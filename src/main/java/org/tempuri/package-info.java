/**
 * Este paquete contiene las clases que fueron autogeneradas por ws-import para
 * implementar la autenticación de usuarios a través del Portal Web de Aplicaciones.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://tempuri.org/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package org.tempuri;
