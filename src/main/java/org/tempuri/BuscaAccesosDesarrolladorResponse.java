
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BuscaAccesosDesarrolladorResult" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "buscaAccesosDesarrolladorResult"
})
@XmlRootElement(name = "BuscaAccesosDesarrolladorResponse")
public class BuscaAccesosDesarrolladorResponse {

	/**
	 *
	 */
	@XmlElement(name = "BuscaAccesosDesarrolladorResult")
    protected ArrayOfString buscaAccesosDesarrolladorResult;

    /**
     * Gets the value of the buscaAccesosDesarrolladorResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getBuscaAccesosDesarrolladorResult() {
        return buscaAccesosDesarrolladorResult;
    }

    /**
     * Sets the value of the buscaAccesosDesarrolladorResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setBuscaAccesosDesarrolladorResult(ArrayOfString value) {
        this.buscaAccesosDesarrolladorResult = value;
    }

}
