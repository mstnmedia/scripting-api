
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_App" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ingenieros" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/&gt;
 *         &lt;element name="cant" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vApp",
    "vIngenieros",
    "cant"
})
@XmlRootElement(name = "InsertaDesarrollador")
public class InsertaDesarrollador {

	/**
	 *
	 */
	@XmlElement(name = "v_App")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_ingenieros")
    protected ArrayOfString vIngenieros;

	/**
	 *
	 */
	protected int cant;

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vIngenieros property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getVIngenieros() {
        return vIngenieros;
    }

    /**
     * Sets the value of the vIngenieros property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setVIngenieros(ArrayOfString value) {
        this.vIngenieros = value;
    }

    /**
     * Gets the value of the cant property.
     * 
	 * @return 
     */
    public int getCant() {
        return cant;
    }

    /**
     * Sets the value of the cant property.
     * 
	 * @param value
     */
    public void setCant(int value) {
        this.cant = value;
    }

}
