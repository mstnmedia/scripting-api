
package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UsoPasswordResponse }
     * 
	 * @return 
     */
    public UsoPasswordResponse createUsoPasswordResponse() {
        return new UsoPasswordResponse();
    }

    /**
     * Create an instance of {@link DelPrimerPass }
     * 
	 * @return 
     */
    public DelPrimerPass createDelPrimerPass() {
        return new DelPrimerPass();
    }

    /**
     * Create an instance of {@link AutenticaTokenResponse }
     * 
	 * @return 
     */
    public AutenticaTokenResponse createAutenticaTokenResponse() {
        return new AutenticaTokenResponse();
    }

    /**
     * Create an instance of {@link EliminaLogsResponse }
     * 
	 * @return 
     */
    public EliminaLogsResponse createEliminaLogsResponse() {
        return new EliminaLogsResponse();
    }

    /**
     * Create an instance of {@link BuscaRolDescripcion }
     * 
	 * @return 
     */
    public BuscaRolDescripcion createBuscaRolDescripcion() {
        return new BuscaRolDescripcion();
    }

    /**
     * Create an instance of {@link GetUsersAppsResponse }
     * 
	 * @return 
     */
    public GetUsersAppsResponse createGetUsersAppsResponse() {
        return new GetUsersAppsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfLocalidadAsociadoSGA }
     * 
	 * @return 
     */
    public ArrayOfLocalidadAsociadoSGA createArrayOfLocalidadAsociadoSGA() {
        return new ArrayOfLocalidadAsociadoSGA();
    }

    /**
     * Create an instance of {@link InsertaLogUsuSalidaResponse }
     * 
	 * @return 
     */
    public InsertaLogUsuSalidaResponse createInsertaLogUsuSalidaResponse() {
        return new InsertaLogUsuSalidaResponse();
    }

    /**
     * Create an instance of {@link DeleteMenuAppsResponse }
     * 
	 * @return 
     */
    public DeleteMenuAppsResponse createDeleteMenuAppsResponse() {
        return new DeleteMenuAppsResponse();
    }

    /**
     * Create an instance of {@link UpdateRolAppsResponse }
     * 
	 * @return 
     */
    public UpdateRolAppsResponse createUpdateRolAppsResponse() {
        return new UpdateRolAppsResponse();
    }

    /**
     * Create an instance of {@link CheckUser }
     * 
	 * @return 
     */
    public CheckUser createCheckUser() {
        return new CheckUser();
    }

    /**
     * Create an instance of {@link DeleteNoticia }
     * 
	 * @return 
     */
    public DeleteNoticia createDeleteNoticia() {
        return new DeleteNoticia();
    }

    /**
     * Create an instance of {@link InsertaDesarrollador }
     * 
	 * @return 
     */
    public InsertaDesarrollador createInsertaDesarrollador() {
        return new InsertaDesarrollador();
    }

    /**
     * Create an instance of {@link DBConnectResponse }
     * 
	 * @return 
     */
    public DBConnectResponse createDBConnectResponse() {
        return new DBConnectResponse();
    }

    /**
     * Create an instance of {@link BuscaRol }
     * 
	 * @return 
     */
    public BuscaRol createBuscaRol() {
        return new BuscaRol();
    }

    /**
     * Create an instance of {@link UpdateUser }
     * 
	 * @return 
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link EliminaAccesoAppsResponse }
     * 
	 * @return 
     */
    public EliminaAccesoAppsResponse createEliminaAccesoAppsResponse() {
        return new EliminaAccesoAppsResponse();
    }

    /**
     * Create an instance of {@link DeleteMenuApps }
     * 
	 * @return 
     */
    public DeleteMenuApps createDeleteMenuApps() {
        return new DeleteMenuApps();
    }

    /**
     * Create an instance of {@link GetUserInfResponse }
     * 
	 * @return 
     */
    public GetUserInfResponse createGetUserInfResponse() {
        return new GetUserInfResponse();
    }

    /**
     * Create an instance of {@link UpdateAppsResponse }
     * 
	 * @return 
     */
    public UpdateAppsResponse createUpdateAppsResponse() {
        return new UpdateAppsResponse();
    }

    /**
     * Create an instance of {@link PAWDBConnect }
     * 
	 * @return 
     */
    public PAWDBConnect createPAWDBConnect() {
        return new PAWDBConnect();
    }

    /**
     * Create an instance of {@link AutenticaResponse }
     * 
	 * @return 
     */
    public AutenticaResponse createAutenticaResponse() {
        return new AutenticaResponse();
    }

    /**
     * Create an instance of {@link BuscaNoticias }
     * 
	 * @return 
     */
    public BuscaNoticias createBuscaNoticias() {
        return new BuscaNoticias();
    }

    /**
     * Create an instance of {@link CheckUserResponse }
     * 
	 * @return 
     */
    public CheckUserResponse createCheckUserResponse() {
        return new CheckUserResponse();
    }

    /**
     * Create an instance of {@link AsociadoSGA }
     * 
	 * @return 
     */
    public AsociadoSGA createAsociadoSGA() {
        return new AsociadoSGA();
    }

    /**
     * Create an instance of {@link GetUserInf }
     * 
	 * @return 
     */
    public GetUserInf createGetUserInf() {
        return new GetUserInf();
    }

    /**
     * Create an instance of {@link GrabaHistPsw }
     * 
	 * @return 
     */
    public GrabaHistPsw createGrabaHistPsw() {
        return new GrabaHistPsw();
    }

    /**
     * Create an instance of {@link URLRetornoResponse }
     * 
	 * @return 
     */
    public URLRetornoResponse createURLRetornoResponse() {
        return new URLRetornoResponse();
    }

    /**
     * Create an instance of {@link InsertaLogUsu }
     * 
	 * @return 
     */
    public InsertaLogUsu createInsertaLogUsu() {
        return new InsertaLogUsu();
    }

    /**
     * Create an instance of {@link GetUltToken }
     * 
	 * @return 
     */
    public GetUltToken createGetUltToken() {
        return new GetUltToken();
    }

    /**
     * Create an instance of {@link DelPrimerPassResponse }
     * 
	 * @return 
     */
    public DelPrimerPassResponse createDelPrimerPassResponse() {
        return new DelPrimerPassResponse();
    }

    /**
     * Create an instance of {@link BuscaAccesosDesarrollador }
     * 
	 * @return 
     */
    public BuscaAccesosDesarrollador createBuscaAccesosDesarrollador() {
        return new BuscaAccesosDesarrollador();
    }

    /**
     * Create an instance of {@link UpdateRolApps }
     * 
	 * @return 
     */
    public UpdateRolApps createUpdateRolApps() {
        return new UpdateRolApps();
    }

    /**
     * Create an instance of {@link DeleteNoticiaResponse }
     * 
	 * @return 
     */
    public DeleteNoticiaResponse createDeleteNoticiaResponse() {
        return new DeleteNoticiaResponse();
    }

    /**
     * Create an instance of {@link InsertaLogUsuSalida }
     * 
	 * @return 
     */
    public InsertaLogUsuSalida createInsertaLogUsuSalida() {
        return new InsertaLogUsuSalida();
    }

    /**
     * Create an instance of {@link EliminaAccesoApps }
     * 
	 * @return 
     */
    public EliminaAccesoApps createEliminaAccesoApps() {
        return new EliminaAccesoApps();
    }

    /**
     * Create an instance of {@link URLRetorno }
     * 
	 * @return 
     */
    public URLRetorno createURLRetorno() {
        return new URLRetorno();
    }

    /**
     * Create an instance of {@link FndPswd }
     * 
	 * @return 
     */
    public FndPswd createFndPswd() {
        return new FndPswd();
    }

    /**
     * Create an instance of {@link UpdateMenuAppsResponse }
     * 
	 * @return 
     */
    public UpdateMenuAppsResponse createUpdateMenuAppsResponse() {
        return new UpdateMenuAppsResponse();
    }

    /**
     * Create an instance of {@link DeleteRolAppsResponse }
     * 
	 * @return 
     */
    public DeleteRolAppsResponse createDeleteRolAppsResponse() {
        return new DeleteRolAppsResponse();
    }

    /**
     * Create an instance of {@link CheckPasswordResponse }
     * 
	 * @return 
     */
    public CheckPasswordResponse createCheckPasswordResponse() {
        return new CheckPasswordResponse();
    }

    /**
     * Create an instance of {@link BuscaNoticiasResponse }
     * 
	 * @return 
     */
    public BuscaNoticiasResponse createBuscaNoticiasResponse() {
        return new BuscaNoticiasResponse();
    }

    /**
     * Create an instance of {@link Autentica }
     * 
	 * @return 
     */
    public Autentica createAutentica() {
        return new Autentica();
    }

    /**
     * Create an instance of {@link InsertaAcceso }
     * 
	 * @return 
     */
    public InsertaAcceso createInsertaAcceso() {
        return new InsertaAcceso();
    }

    /**
     * Create an instance of {@link GetParamSecurity }
     * 
	 * @return 
     */
    public GetParamSecurity createGetParamSecurity() {
        return new GetParamSecurity();
    }

    /**
     * Create an instance of {@link GetUltAcceso }
     * 
	 * @return 
     */
    public GetUltAcceso createGetUltAcceso() {
        return new GetUltAcceso();
    }

    /**
     * Create an instance of {@link EliminaApps }
     * 
	 * @return 
     */
    public EliminaApps createEliminaApps() {
        return new EliminaApps();
    }

    /**
     * Create an instance of {@link SiTienePassword }
     * 
	 * @return 
     */
    public SiTienePassword createSiTienePassword() {
        return new SiTienePassword();
    }

    /**
     * Create an instance of {@link UpdateCuenta }
     * 
	 * @return 
     */
    public UpdateCuenta createUpdateCuenta() {
        return new UpdateCuenta();
    }

    /**
     * Create an instance of {@link CheckPassword }
     * 
	 * @return 
     */
    public CheckPassword createCheckPassword() {
        return new CheckPassword();
    }

    /**
     * Create an instance of {@link GetAsociadoSGAResponse }
     * 
	 * @return 
     */
    public GetAsociadoSGAResponse createGetAsociadoSGAResponse() {
        return new GetAsociadoSGAResponse();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
	 * @return 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link UpdateUltAcceso }
     * 
	 * @return 
     */
    public UpdateUltAcceso createUpdateUltAcceso() {
        return new UpdateUltAcceso();
    }

    /**
     * Create an instance of {@link UpdateParamSec }
     * 
	 * @return 
     */
    public UpdateParamSec createUpdateParamSec() {
        return new UpdateParamSec();
    }

    /**
     * Create an instance of {@link SendMailResponse }
     * 
	 * @return 
     */
    public SendMailResponse createSendMailResponse() {
        return new SendMailResponse();
    }

    /**
     * Create an instance of {@link UpdateCuentaResponse }
     * 
	 * @return 
     */
    public UpdateCuentaResponse createUpdateCuentaResponse() {
        return new UpdateCuentaResponse();
    }

    /**
     * Create an instance of {@link PAWDBConnectResponse }
     * 
	 * @return 
     */
    public PAWDBConnectResponse createPAWDBConnectResponse() {
        return new PAWDBConnectResponse();
    }

    /**
     * Create an instance of {@link BuscaRolDescripcionResponse }
     * 
	 * @return 
     */
    public BuscaRolDescripcionResponse createBuscaRolDescripcionResponse() {
        return new BuscaRolDescripcionResponse();
    }

    /**
     * Create an instance of {@link BuscaAccesos }
     * 
	 * @return 
     */
    public BuscaAccesos createBuscaAccesos() {
        return new BuscaAccesos();
    }

    /**
     * Create an instance of {@link AccesoOpcionResponse }
     * 
	 * @return 
     */
    public AccesoOpcionResponse createAccesoOpcionResponse() {
        return new AccesoOpcionResponse();
    }

    /**
     * Create an instance of {@link CambiaPassResponse }
     * 
	 * @return 
     */
    public CambiaPassResponse createCambiaPassResponse() {
        return new CambiaPassResponse();
    }

    /**
     * Create an instance of {@link GrabaHistPswResponse }
     * 
	 * @return 
     */
    public GrabaHistPswResponse createGrabaHistPswResponse() {
        return new GrabaHistPswResponse();
    }

    /**
     * Create an instance of {@link UpdateApps }
     * 
	 * @return 
     */
    public UpdateApps createUpdateApps() {
        return new UpdateApps();
    }

    /**
     * Create an instance of {@link FndPswdResponse }
     * 
	 * @return 
     */
    public FndPswdResponse createFndPswdResponse() {
        return new FndPswdResponse();
    }

    /**
     * Create an instance of {@link EliminaLogs }
     * 
	 * @return 
     */
    public EliminaLogs createEliminaLogs() {
        return new EliminaLogs();
    }

    /**
     * Create an instance of {@link BuscaAccesosResponse }
     * 
	 * @return 
     */
    public BuscaAccesosResponse createBuscaAccesosResponse() {
        return new BuscaAccesosResponse();
    }

    /**
     * Create an instance of {@link UsoPassword }
     * 
	 * @return 
     */
    public UsoPassword createUsoPassword() {
        return new UsoPassword();
    }

    /**
     * Create an instance of {@link DBConnect }
     * 
	 * @return 
     */
    public DBConnect createDBConnect() {
        return new DBConnect();
    }

    /**
     * Create an instance of {@link UpdateMenuApps }
     * 
	 * @return 
     */
    public UpdateMenuApps createUpdateMenuApps() {
        return new UpdateMenuApps();
    }

    /**
     * Create an instance of {@link InsertaDesarrolladorResponse }
     * 
	 * @return 
     */
    public InsertaDesarrolladorResponse createInsertaDesarrolladorResponse() {
        return new InsertaDesarrolladorResponse();
    }

    /**
     * Create an instance of {@link AccesoOpcion }
     * 
	 * @return 
     */
    public AccesoOpcion createAccesoOpcion() {
        return new AccesoOpcion();
    }

    /**
     * Create an instance of {@link BuscaAccesosDesarrolladorResponse }
     * 
	 * @return 
     */
    public BuscaAccesosDesarrolladorResponse createBuscaAccesosDesarrolladorResponse() {
        return new BuscaAccesosDesarrolladorResponse();
    }

    /**
     * Create an instance of {@link BuscaAccesosDesc }
     * 
	 * @return 
     */
    public BuscaAccesosDesc createBuscaAccesosDesc() {
        return new BuscaAccesosDesc();
    }

    /**
     * Create an instance of {@link StrConexionResponse }
     * 
	 * @return 
     */
    public StrConexionResponse createStrConexionResponse() {
        return new StrConexionResponse();
    }

    /**
     * Create an instance of {@link InsertaAccesoApps }
     * 
	 * @return 
     */
    public InsertaAccesoApps createInsertaAccesoApps() {
        return new InsertaAccesoApps();
    }

    /**
     * Create an instance of {@link SiTienePasswordResponse }
     * 
	 * @return 
     */
    public SiTienePasswordResponse createSiTienePasswordResponse() {
        return new SiTienePasswordResponse();
    }

    /**
     * Create an instance of {@link GetUsersApps }
     * 
	 * @return 
     */
    public GetUsersApps createGetUsersApps() {
        return new GetUsersApps();
    }

    /**
     * Create an instance of {@link SendMail }
     * 
	 * @return 
     */
    public SendMail createSendMail() {
        return new SendMail();
    }

    /**
     * Create an instance of {@link InsertaAccesoAppsResponse }
     * 
	 * @return 
     */
    public InsertaAccesoAppsResponse createInsertaAccesoAppsResponse() {
        return new InsertaAccesoAppsResponse();
    }

    /**
     * Create an instance of {@link AutenticaToken }
     * 
	 * @return 
     */
    public AutenticaToken createAutenticaToken() {
        return new AutenticaToken();
    }

    /**
     * Create an instance of {@link BuscaRolResponse }
     * 
	 * @return 
     */
    public BuscaRolResponse createBuscaRolResponse() {
        return new BuscaRolResponse();
    }

    /**
     * Create an instance of {@link BuscaAccesosDescResponse }
     * 
	 * @return 
     */
    public BuscaAccesosDescResponse createBuscaAccesosDescResponse() {
        return new BuscaAccesosDescResponse();
    }

    /**
     * Create an instance of {@link InsertaLogUsuResponse }
     * 
	 * @return 
     */
    public InsertaLogUsuResponse createInsertaLogUsuResponse() {
        return new InsertaLogUsuResponse();
    }

    /**
     * Create an instance of {@link GetUltTokenResponse }
     * 
	 * @return 
     */
    public GetUltTokenResponse createGetUltTokenResponse() {
        return new GetUltTokenResponse();
    }

    /**
     * Create an instance of {@link GetDescripcionRolResponse }
     * 
	 * @return 
     */
    public GetDescripcionRolResponse createGetDescripcionRolResponse() {
        return new GetDescripcionRolResponse();
    }

    /**
     * Create an instance of {@link GetAsociadoSGA }
     * 
	 * @return 
     */
    public GetAsociadoSGA createGetAsociadoSGA() {
        return new GetAsociadoSGA();
    }

    /**
     * Create an instance of {@link GetUsuarioPorTarjeta }
     * 
	 * @return 
     */
    public GetUsuarioPorTarjeta createGetUsuarioPorTarjeta() {
        return new GetUsuarioPorTarjeta();
    }

    /**
     * Create an instance of {@link UpdateParamSecResponse }
     * 
	 * @return 
     */
    public UpdateParamSecResponse createUpdateParamSecResponse() {
        return new UpdateParamSecResponse();
    }

    /**
     * Create an instance of {@link LocalidadAsociadoSGA }
     * 
	 * @return 
     */
    public LocalidadAsociadoSGA createLocalidadAsociadoSGA() {
        return new LocalidadAsociadoSGA();
    }

    /**
     * Create an instance of {@link EliminaAppsResponse }
     * 
	 * @return 
     */
    public EliminaAppsResponse createEliminaAppsResponse() {
        return new EliminaAppsResponse();
    }

    /**
     * Create an instance of {@link GetDescripcionRol }
     * 
	 * @return 
     */
    public GetDescripcionRol createGetDescripcionRol() {
        return new GetDescripcionRol();
    }

    /**
     * Create an instance of {@link DeleteUserResponse }
     * 
	 * @return 
     */
    public DeleteUserResponse createDeleteUserResponse() {
        return new DeleteUserResponse();
    }

    /**
     * Create an instance of {@link StrConexion }
     * 
	 * @return 
     */
    public StrConexion createStrConexion() {
        return new StrConexion();
    }

    /**
     * Create an instance of {@link GetUsuarioPorTarjetaResponse }
     * 
	 * @return 
     */
    public GetUsuarioPorTarjetaResponse createGetUsuarioPorTarjetaResponse() {
        return new GetUsuarioPorTarjetaResponse();
    }

    /**
     * Create an instance of {@link UpdateNoticia }
     * 
	 * @return 
     */
    public UpdateNoticia createUpdateNoticia() {
        return new UpdateNoticia();
    }

    /**
     * Create an instance of {@link UpdateUltAccesoResponse }
     * 
	 * @return 
     */
    public UpdateUltAccesoResponse createUpdateUltAccesoResponse() {
        return new UpdateUltAccesoResponse();
    }

    /**
     * Create an instance of {@link InsertaAccesoResponse }
     * 
	 * @return 
     */
    public InsertaAccesoResponse createInsertaAccesoResponse() {
        return new InsertaAccesoResponse();
    }

    /**
     * Create an instance of {@link GetUltAccesoResponse }
     * 
	 * @return 
     */
    public GetUltAccesoResponse createGetUltAccesoResponse() {
        return new GetUltAccesoResponse();
    }

    /**
     * Create an instance of {@link UpdateNoticiaResponse }
     * 
	 * @return 
     */
    public UpdateNoticiaResponse createUpdateNoticiaResponse() {
        return new UpdateNoticiaResponse();
    }

    /**
     * Create an instance of {@link DeleteRolApps }
     * 
	 * @return 
     */
    public DeleteRolApps createDeleteRolApps() {
        return new DeleteRolApps();
    }

    /**
     * Create an instance of {@link CambiaPass }
     * 
	 * @return 
     */
    public CambiaPass createCambiaPass() {
        return new CambiaPass();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
	 * @return 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link GetParamSecurityResponse }
     * 
	 * @return 
     */
    public GetParamSecurityResponse createGetParamSecurityResponse() {
        return new GetParamSecurityResponse();
    }

    /**
     * Create an instance of {@link DeleteUser }
     * 
	 * @return 
     */
    public DeleteUser createDeleteUser() {
        return new DeleteUser();
    }

}
