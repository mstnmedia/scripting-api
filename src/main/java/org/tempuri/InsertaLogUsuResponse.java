
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InsertaLogUsuResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertaLogUsuResult"
})
@XmlRootElement(name = "InsertaLogUsuResponse")
public class InsertaLogUsuResponse {

	/**
	 *
	 */
	@XmlElement(name = "InsertaLogUsuResult")
    protected boolean insertaLogUsuResult;

    /**
     * Gets the value of the insertaLogUsuResult property.
     * 
	 * @return 
     */
    public boolean isInsertaLogUsuResult() {
        return insertaLogUsuResult;
    }

    /**
     * Sets the value of the insertaLogUsuResult property.
     * 
	 * @param value
     */
    public void setInsertaLogUsuResult(boolean value) {
        this.insertaLogUsuResult = value;
    }

}
