
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_nom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vId",
    "vNom",
    "vDesc",
    "vTipo",
    "vUsuario",
    "vIp"
})
@XmlRootElement(name = "UpdateMenuApps")
public class UpdateMenuApps {

	/**
	 *
	 */
	@XmlElement(name = "v_id")
    protected String vId;

	/**
	 *
	 */
	@XmlElement(name = "v_nom")
    protected String vNom;

	/**
	 *
	 */
	@XmlElement(name = "v_desc")
    protected String vDesc;

	/**
	 *
	 */
	@XmlElement(name = "v_tipo")
    protected String vTipo;

	/**
	 *
	 */
	@XmlElement(name = "v_usuario")
    protected String vUsuario;

	/**
	 *
	 */
	@XmlElement(name = "v_ip")
    protected String vIp;

    /**
     * Gets the value of the vId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVId() {
        return vId;
    }

    /**
     * Sets the value of the vId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVId(String value) {
        this.vId = value;
    }

    /**
     * Gets the value of the vNom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVNom() {
        return vNom;
    }

    /**
     * Sets the value of the vNom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVNom(String value) {
        this.vNom = value;
    }

    /**
     * Gets the value of the vDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVDesc() {
        return vDesc;
    }

    /**
     * Sets the value of the vDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVDesc(String value) {
        this.vDesc = value;
    }

    /**
     * Gets the value of the vTipo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVTipo() {
        return vTipo;
    }

    /**
     * Sets the value of the vTipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVTipo(String value) {
        this.vTipo = value;
    }

    /**
     * Gets the value of the vUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsuario() {
        return vUsuario;
    }

    /**
     * Sets the value of the vUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsuario(String value) {
        this.vUsuario = value;
    }

    /**
     * Gets the value of the vIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIp() {
        return vIp;
    }

    /**
     * Sets the value of the vIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIp(String value) {
        this.vIp = value;
    }

}
