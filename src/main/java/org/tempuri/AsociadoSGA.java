
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AsociadoSGA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AsociadoSGA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodAsociado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Correo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Localidades" type="{http://tempuri.org/}ArrayOfLocalidadAsociadoSGA" minOccurs="0"/&gt;
 *         &lt;element name="ErrorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ErrorMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AsociadoSGA", propOrder = {
    "codAsociado",
    "nombre",
    "correo",
    "localidades",
    "errorCode",
    "errorMsg"
})
public class AsociadoSGA {

	/**
	 *
	 */
	@XmlElement(name = "CodAsociado")
    protected String codAsociado;

	/**
	 *
	 */
	@XmlElement(name = "Nombre")
    protected String nombre;

	/**
	 *
	 */
	@XmlElement(name = "Correo")
    protected String correo;

	/**
	 *
	 */
	@XmlElement(name = "Localidades")
    protected ArrayOfLocalidadAsociadoSGA localidades;

	/**
	 *
	 */
	@XmlElement(name = "ErrorCode")
    protected String errorCode;

	/**
	 *
	 */
	@XmlElement(name = "ErrorMsg")
    protected String errorMsg;

    /**
     * Gets the value of the codAsociado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAsociado() {
        return codAsociado;
    }

    /**
     * Sets the value of the codAsociado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAsociado(String value) {
        this.codAsociado = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the correo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Sets the value of the correo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreo(String value) {
        this.correo = value;
    }

    /**
     * Gets the value of the localidades property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLocalidadAsociadoSGA }
     *     
     */
    public ArrayOfLocalidadAsociadoSGA getLocalidades() {
        return localidades;
    }

    /**
     * Sets the value of the localidades property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLocalidadAsociadoSGA }
     *     
     */
    public void setLocalidades(ArrayOfLocalidadAsociadoSGA value) {
        this.localidades = value;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * Sets the value of the errorMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMsg(String value) {
        this.errorMsg = value;
    }

}
