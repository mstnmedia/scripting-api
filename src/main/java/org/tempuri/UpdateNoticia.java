
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_sec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ini" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_fin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_not" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vSec",
    "vIni",
    "vFin",
    "vNot",
    "vUsuario",
    "vIp"
})
@XmlRootElement(name = "UpdateNoticia")
public class UpdateNoticia {

	/**
	 *
	 */
	@XmlElement(name = "v_sec")
    protected String vSec;

	/**
	 *
	 */
	@XmlElement(name = "v_ini")
    protected String vIni;

	/**
	 *
	 */
	@XmlElement(name = "v_fin")
    protected String vFin;

	/**
	 *
	 */
	@XmlElement(name = "v_not")
    protected String vNot;

	/**
	 *
	 */
	@XmlElement(name = "v_usuario")
    protected String vUsuario;

	/**
	 *
	 */
	@XmlElement(name = "v_ip")
    protected String vIp;

    /**
     * Gets the value of the vSec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVSec() {
        return vSec;
    }

    /**
     * Sets the value of the vSec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVSec(String value) {
        this.vSec = value;
    }

    /**
     * Gets the value of the vIni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIni() {
        return vIni;
    }

    /**
     * Sets the value of the vIni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIni(String value) {
        this.vIni = value;
    }

    /**
     * Gets the value of the vFin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVFin() {
        return vFin;
    }

    /**
     * Sets the value of the vFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVFin(String value) {
        this.vFin = value;
    }

    /**
     * Gets the value of the vNot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVNot() {
        return vNot;
    }

    /**
     * Sets the value of the vNot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVNot(String value) {
        this.vNot = value;
    }

    /**
     * Gets the value of the vUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsuario() {
        return vUsuario;
    }

    /**
     * Sets the value of the vUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsuario(String value) {
        this.vUsuario = value;
    }

    /**
     * Gets the value of the vIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIp() {
        return vIp;
    }

    /**
     * Sets the value of the vIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIp(String value) {
        this.vIp = value;
    }

}
