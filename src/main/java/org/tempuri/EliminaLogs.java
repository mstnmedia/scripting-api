
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="v_fec1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_fec2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_app" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="v_usu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vFec1",
    "vFec2",
    "vApp",
    "vUsu"
})
@XmlRootElement(name = "EliminaLogs")
public class EliminaLogs {

	/**
	 *
	 */
	@XmlElement(name = "v_fec1")
    protected String vFec1;

	/**
	 *
	 */
	@XmlElement(name = "v_fec2")
    protected String vFec2;

	/**
	 *
	 */
	@XmlElement(name = "v_app")
    protected String vApp;

	/**
	 *
	 */
	@XmlElement(name = "v_usu")
    protected String vUsu;

    /**
     * Gets the value of the vFec1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVFec1() {
        return vFec1;
    }

    /**
     * Sets the value of the vFec1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVFec1(String value) {
        this.vFec1 = value;
    }

    /**
     * Gets the value of the vFec2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVFec2() {
        return vFec2;
    }

    /**
     * Sets the value of the vFec2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVFec2(String value) {
        this.vFec2 = value;
    }

    /**
     * Gets the value of the vApp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVApp() {
        return vApp;
    }

    /**
     * Sets the value of the vApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVApp(String value) {
        this.vApp = value;
    }

    /**
     * Gets the value of the vUsu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUsu() {
        return vUsu;
    }

    /**
     * Sets the value of the vUsu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUsu(String value) {
        this.vUsu = value;
    }

}
