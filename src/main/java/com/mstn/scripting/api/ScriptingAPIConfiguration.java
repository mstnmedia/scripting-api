package com.mstn.scripting.api;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mstn.scripting.api.core.MicroservicesFactory;
import io.dropwizard.client.HttpClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Clase que define la configuración del microservicio API.
 *
 * @author MSTN Media
 */
public class ScriptingAPIConfiguration extends Configuration {

	/**
	 *
	 */
	static public ScriptingAPIConfiguration current = null;

	@NotEmpty
	private String wsdlPAW;

	/**
	 *
	 * @return
	 */
	@JsonProperty("wsdlPAW")
	public String getWsdlURL() {
		return wsdlPAW;
	}

	/**
	 *
	 * @param wsdlPAW
	 */
	@JsonProperty("wsdlPAW")
	public void setWsdlURL(String wsdlPAW) {
		this.wsdlPAW = wsdlPAW;
	}

	@NotEmpty
	private String appURL;

	/**
	 *
	 * @return
	 */
	@JsonProperty("appURL")
	public String getAppURL() {
		return appURL;
	}

	/**
	 *
	 * @param appURL
	 */
	@JsonProperty("appURL")
	public void setAppURL(String appURL) {
		this.appURL = appURL;
	}

	@NotEmpty
	private String cookiePath;

	/**
	 *
	 * @return
	 */
	@JsonProperty("cookiePath")
	public String getCookiePath() {
		return cookiePath;
	}

	/**
	 *
	 * @param cookiePath
	 */
	@JsonProperty("cookiePath")
	public void setCookiePath(String cookiePath) {
		this.cookiePath = cookiePath;
	}

	@NotEmpty
	private String uploadFolder;

	/**
	 *
	 * @return
	 */
	@JsonProperty("uploadFolder")
	public String getUploadFolder() {
		return uploadFolder;
	}

	/**
	 *
	 * @param uploadFolder
	 */
	@JsonProperty("uploadFolder")
	public void setUploadFolder(String uploadFolder) {
		this.uploadFolder = uploadFolder;
	}

	@NotEmpty
	private String contentsFolder;

	/**
	 *
	 * @return
	 */
	@JsonProperty("contentsFolder")
	public String getContentsFolder() {
		return contentsFolder;
	}

	/**
	 *
	 * @param contentsFolder
	 */
	@JsonProperty("contentsFolder")
	public void setContentsFolder(String contentsFolder) {
		this.contentsFolder = contentsFolder;
	}

	private boolean debugMode;

	/**
	 *
	 * @return
	 */
	@JsonProperty("debugMode")
	public boolean getDebugMode() {
		return debugMode;
	}

	private int debugUser;

	/**
	 *
	 * @return
	 */
	@JsonProperty("debugUser")
	public int getDebugUser() {
		return debugUser;
	}

	private int debugRole;

	/**
	 *
	 * @return
	 */
	@JsonProperty("debugRole")
	public int getDebugRole() {
		return debugRole;
	}

	@NotNull
	private String portalURL;

	/**
	 *
	 * @return
	 */
	@JsonProperty("portalURL")
	public String getPortalURL() {
		return portalURL;
	}

	/**
	 *
	 * @param portalURL
	 */
	@JsonProperty("portalURL")
	public void setPortalURL(String portalURL) {
		this.portalURL = portalURL;
	}
	@Valid
	@NotNull
	private HttpClientConfiguration httpClient = new HttpClientConfiguration();

	/**
	 *
	 * @return
	 */
	@JsonProperty("httpClient")
	public HttpClientConfiguration getHttpClientConfiguration() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 */
	@JsonProperty("httpClient")
	public void setHttpClientConfiguration(HttpClientConfiguration httpClient) {
		this.httpClient = httpClient;
	}

	//Microservices Configuration
	@NotEmpty
	private Map<String, MicroservicesFactory> args;

	/**
	 *
	 * @return
	 */
	@JsonProperty("microservices")
	public Map<String, MicroservicesFactory> getMicroservices() {
		return args;
	}

	/**
	 *
	 * @param args
	 */
	@JsonProperty("microservices")
	public void setMicroservices(Map<String, MicroservicesFactory> args) {
		this.args = args;
	}

	final String DATABASE = "database";
	@Valid
	@NotNull
	private DataSourceFactory dataSourceFactory = new DataSourceFactory();

	/**
	 *
	 * @return
	 */
	@JsonProperty(DATABASE)
	public DataSourceFactory getDataSourceFactory() {
		return dataSourceFactory;
	}

	/**
	 *
	 * @param dataSourceFactory
	 */
	@JsonProperty(DATABASE)
	public void setDataSourceFactory(final DataSourceFactory dataSourceFactory) {
		this.dataSourceFactory = dataSourceFactory;
	}

	private int tokenTime;

	/**
	 *
	 * @return
	 */
	@JsonProperty("tokenTime")
	public int getTokenTime() {
		return tokenTime;
	}

	@NotEmpty
	private String timeZone;

	/**
	 *
	 * @return
	 */
	@JsonProperty("timeZone")
	public String getTimeZone() {
		return timeZone;
	}

	@NotEmpty
	private String locale;

	/**
	 *
	 * @return
	 */
	@JsonProperty("locale")
	public String getLocale() {
		return locale;
	}
}
