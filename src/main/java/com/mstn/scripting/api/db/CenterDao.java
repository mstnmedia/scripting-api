/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Center;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code center}.
 *
 * @author josesuero
 */
@RegisterMapper(CenterDao.CenterMapper.class)
@UseStringTemplate3StatementLocator
public abstract class CenterDao {

	/**
	 *
	 */
	static public class CenterMapper implements ResultSetMapper<Center> {

		private static final String ID = "id";
		private static final String NAME = "name";
		private static final String ID_EMPLOYEE = "id_employee";
		private static final String DELETED = "deleted";
		private static final String NOTES = "notes";
		private static final String EMPLOYEE_NAME = "employee_name";

		@Override
		public Center map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Center(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getInt(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(DELETED) ? resultSet.getBoolean(DELETED) : false,
					hasColumn.containsKey(NOTES) ? resultSet.getString(NOTES) : "",
					hasColumn.containsKey(EMPLOYEE_NAME) ? resultSet.getString(EMPLOYEE_NAME) : ""
			);
		}
	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_center <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_center <where> order by ID desc")
	abstract public List<Center> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Center> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, t.* from ("
			+ "  select t.* from v_center t <where> order by <order_by>"
			+ " ) t"
			+ ") t where t.rowno >= ((:page_number - 1) * :page_size + 1) and t.rowno \\<= (:page_number * :page_size)")
	abstract public List<Center> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	// TODO: order_by debe ser una lista como WhereClause
	);

	/**
	 *
	 * @param id_employee
	 * @return
	 */
	@SqlQuery("select 1 centerID from dual")
	abstract public int getEmployeeCenterID(@Bind("id_employee") final long id_employee);

	/**
	 *
	 * @param id_employee
	 * @return
	 */
	@SqlQuery("select * from v_center where id_employee = :id_employee")
	abstract public Center getByEmployeeID(@Bind("id_employee") final long id_employee);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_center where id = :id")
	abstract public Center get(@Bind("id") final long id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into center(id, name, id_employee, deleted, notes) values(center_seq.nextVal, :name, :id_employee, :deleted, :notes)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Center item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update center set "
			+ "	name = :name, "
			+ "	id_employee = :id_employee, "
			+ "	deleted = :deleted, "
			+ "	notes = :notes "
			+ "where id = :id")
	abstract public void update(@BindBean final Center item);

	/**
	 *
	 * @param id
	 * @param notes
	 */
	@SqlUpdate("update center set notes = :notes where id = :id")
	abstract public void updateNotes(@Bind("id") long id, @Bind("notes") String notes);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from center where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select count(id) value from alert where id_center=:id")
	abstract public long alertDependents(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select count(id) value from category where id_center=:id")
	abstract public long categoryDependents(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select count(id) value from criteria where id_center=:id")
	abstract public long criteriaDependents(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select count(id) value from end_call where id_center=:id")
	abstract public long endCallDependents(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select count(id) value from content where id_center=:id")
	abstract public long contentDependents(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select count(id) value from transaction where id_center=:id")
	abstract public long transactionDependents(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select count(id) value from workflow where id_center=:id")
	abstract public long workflowDependents(@Bind("id") int id);

}
