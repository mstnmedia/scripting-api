/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.ReportFilter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code report_filter}.
 *
 * @author amatos
 */
@RegisterMapper(ReportFilterDao.ReportFilterMapper.class)
@UseStringTemplate3StatementLocator
public abstract class ReportFilterDao {

	static public class ReportFilterMapper implements ResultSetMapper<ReportFilter> {

		private static final String ID = "id";
		private static final String ID_REPORT = "id_report";
		private static final String ID_EMPLOYEE = "id_employee";
		private static final String NAME = "name";
		private static final String JSON = "json";

		@Override
		public ReportFilter map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new ReportFilter(
					hasColumn.containsKey(ID) ? resultSet.getLong(ID) : 0,
					hasColumn.containsKey(ID_REPORT) ? resultSet.getLong(ID_REPORT) : 0,
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getLong(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(JSON) ? resultSet.getString(JSON) : ""
			);
		}
	}

	public long getCount() {
		return getCount(null);
	}

	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	@SqlQuery("select count(id) from report_filter <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	@SqlQuery("select * from report_filter <where> order by name")
	abstract public List<ReportFilter> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	public List<ReportFilter> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	@SqlQuery("select * from ("
			+ " select rownum rowno, r.* from ("
			+ "  select r.* from report_filter r <where> order by <order_by>"
			+ " ) r"
			+ ") r where r.rowno >= ((:page_number - 1) * :page_size + 1) and r.rowno \\<= (:page_number * :page_size)")
	abstract public List<ReportFilter> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	@SqlQuery("select * from report_filter where id = :id")
	abstract public ReportFilter get(@Bind("id") final long id);

	@SqlUpdate("insert into report_filter(id, id_report, id_employee, name, json)"
			+ " values(report_filter_seq.nextVal, :id_report, :id_employee, :name, :json)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public long insert(@BindBean final ReportFilter item);

	@SqlUpdate("update report_filter set "
			+ "	id_report = :id_report, "
			+ "	id_employee = :id_employee, "
			+ "	name = :name, "
			+ "	json = :json "
			+ "where id = :id")
	abstract public void update(@BindBean final ReportFilter item);

	@SqlUpdate("delete from report_filter where id = :id")
	abstract public int delete(@Bind("id") final long id);

}
