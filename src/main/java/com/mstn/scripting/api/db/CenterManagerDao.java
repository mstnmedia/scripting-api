/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Center_Manager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla relacional {@code center_manager}.
 *
 * @see CenterDao
 * @see com.mstn.scripting.core.db.common.EmployeeDao
 * @author amatos
 */
@RegisterMapper(CenterManagerDao.CenterManagerMapper.class)
@UseStringTemplate3StatementLocator
public interface CenterManagerDao {

	/**
	 *
	 */
	static public class CenterManagerMapper implements ResultSetMapper<Center_Manager> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_MANAGER = "id_manager";
		private static final String GOAL_SECONDS = "goal_seconds";
		private static final String MARGIN_PERCENTAGE = "margin_percentage";

		private static final String CENTER_NAME = "center_name";
		private static final String MANAGER_NAME = "manager_name";

		@Override
		public Center_Manager map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Center_Manager(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_MANAGER) ? resultSet.getInt(ID_MANAGER) : 0,
					hasColumn.containsKey(GOAL_SECONDS) ? resultSet.getInt(GOAL_SECONDS) : 0,
					hasColumn.containsKey(MARGIN_PERCENTAGE) ? resultSet.getInt(MARGIN_PERCENTAGE) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(MANAGER_NAME) ? resultSet.getString(MANAGER_NAME) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_center_manager cm <where> order by id")
	List<Center_Manager> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_center
	 * @return
	 */
	@SqlQuery("select * from v_center_manager cm where id_center=:id_center order by id")
	List<Center_Manager> getAll(@Bind("id_center") int id_center);

	/**
	 *
	 * @param id_manager
	 * @return
	 */
	@SqlQuery("select * from v_center_manager cm where id_manager=:id_manager order by id")
	List<Center_Manager> getByManagerID(@Bind("id_manager") int id_manager);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_center_manager cm where id = :id")
	Center_Manager get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into center_manager(id, id_center, id_manager, goal_seconds, margin_percentage) "
			+ "values(center_manager_seq.nextVal, :id_center, :id_manager, :goal_seconds, :margin_percentage)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Center_Manager item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update center_manager set"
			+ " id_center = :id_center,"
			+ " id_manager = :id_manager,"
			+ " goal_seconds = :goal_seconds,"
			+ " margin_percentage = :margin_percentage "
			+ "where id = :id")
	void update(@BindBean final Center_Manager item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from center_manager where id = :id")
	int delete(@Bind("id") final int id);
}
