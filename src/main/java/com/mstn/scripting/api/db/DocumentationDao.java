/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.db;

import com.mstn.scripting.core.models.Documentation;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code documentation}.
 *
 * @author amatos
 */
@RegisterMapper(DocumentationDao.DocumentationMapper.class)
@UseStringTemplate3StatementLocator
public abstract class DocumentationDao {

	/**
	 *
	 */
	static public class DocumentationMapper implements ResultSetMapper<Documentation> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String CENTER_NAME = "center_name";
		private static final String TYPE = "id_type";
		private static final String NAME = "name";
		private static final String TAGS = "tags";
		private static final String VALUE = "value";

		@Override
		public Documentation map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Documentation(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(TYPE) ? resultSet.getString(TYPE) : "",
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(TAGS) ? resultSet.getString(TAGS) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from documentation <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Documentation> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, c.* from ("
			+ "  select c.* from v_documentation c <where> order by <order_by>"
			+ " ) c"
			+ ") c where c.rowno >= ((:page_number - 1) * :page_size + 1) and c.rowno \\<= (:page_number * :page_size)")
	abstract public List<Documentation> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_documentation <where> order by id desc")
	abstract public List<Documentation> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_documentation where id = :id")
	abstract public Documentation get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into documentation(id, id_center, id_type, name, tags, value)"
			+ " values(documentation_seq.nextVal, :id_center, coalesce(:id_type, ''), :name, :tags, :value)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Documentation item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update documentation set "
			+ " id_center = :id_center, "
			+ " id_type = :id_type, "
			+ " name = :name, "
			+ " tags = :tags, "
			+ " value = to_clob(:value) "
			+ "where id = :id")
	abstract public void update(@BindBean final Documentation item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from documentation where id = :id")
	abstract public int delete(@Bind("id") final int id);

}
