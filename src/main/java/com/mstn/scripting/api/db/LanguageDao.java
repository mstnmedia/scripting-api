/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la vista {@code v_language}.
 *
 * @author MSTN Media
 */
@RegisterMapper(LanguageDao.LanguageMapper.class)
@UseStringTemplate3StatementLocator
public abstract class LanguageDao {

	/**
	 *
	 */
	static public class LanguageMapper implements ResultSetMapper<Language> {

		private static final String ID = "id";
		private static final String ID_EMPLOYEE = "id_employee";
		private static final String NAME = "name";
		private static final String MAIN = "main";
		private static final String EMPLOYEE_NAME = "employee_name";
		private static final String LANGUAGE_NAME = "language_name";

		@Override
		public Language map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Language(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getInt(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(MAIN) ? resultSet.getInt(MAIN) : 0,
					hasColumn.containsKey(EMPLOYEE_NAME) ? resultSet.getString(EMPLOYEE_NAME) : "",
					hasColumn.containsKey(LANGUAGE_NAME) ? resultSet.getString(LANGUAGE_NAME) : ""
			);
		}

	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_language <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_language <where> order by main, id")
	abstract public List<Language> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param languageName
	 * @return
	 */
	@SqlQuery("select * from v_language where name=:languageName order by main, id")
	abstract public List<Language> getAll(@Bind("languageName") String languageName);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Language> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, l.* from ("
			+ "  select l.* from v_language l <where> order by <order_by>"
			+ " ) l"
			+ ") l where l.rowno >= ((:page_number - 1) * :page_size + 1) and l.rowno \\<= (:page_number * :page_size)")
	abstract public List<Language> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_language where id = :id")
	abstract public Language get(@Bind("id") final int id);

	/**
	 *
	 * @param id_user
	 * @return
	 */
	@SqlQuery("select * from v_language where id_employee = :id_user")
	abstract public Language getUserLanguage(@Bind("id_user") final int id_user);

	/**
	 *
	 * @return
	 */
	@SqlQuery("select * from v_language where id_employee = -1")
	abstract public Language getSystemLanguage();

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into language(id, id_employee, name, main) values(language_seq.nextVal, :id_employee, :name, :main)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Language item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update language set "
			+ " id_employee = :id_employee,"
			+ " name = :name,"
			+ " main = :main "
			+ "where id = :id")
	abstract public void update(@BindBean final Language item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from language where id = :id")
	abstract public int delete(@Bind("id") final int id);
}
