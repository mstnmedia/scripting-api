/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.db;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Clase que define las propiedades de la configuración de idiomas por usuario.
 *
 * @author josesuero
 */
public class Language {

	private int id;
	private int id_employee;
	@NotEmpty
	private String name;
	private int main;
	private String employee_name;
	private String language_name;

	/**
	 *
	 */
	public Language() {

	}

	/**
	 *
	 * @param id
	 * @param id_employee
	 * @param name
	 * @param main
	 * @param employee_name
	 * @param language_name
	 */
	public Language(int id, int id_employee, String name, int main, String employee_name, String language_name) {
		this.id = id;
		this.id_employee = id_employee;
		this.name = name;
		this.main = main;
		this.employee_name = employee_name;
		this.language_name = language_name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the main
	 */
	public int getMain() {
		return main;
	}

	/**
	 * @param main the main to set
	 */
	public void setMain(int main) {
		this.main = main;
	}

	/**
	 *
	 * @return
	 */
	public String getEmployee_name() {
		return employee_name;
	}

	/**
	 *
	 * @param employee_name
	 */
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	/**
	 *
	 * @return
	 */
	public String getLanguage_name() {
		return language_name;
	}

	/**
	 *
	 * @param language_name
	 */
	public void setLanguage_name(String language_name) {
		this.language_name = language_name;
	}

}
