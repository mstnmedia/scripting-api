/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.ClobValue;
import com.mstn.scripting.core.db.ClobValueBinder;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.System_Config;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code system_config}.
 *
 * @author amatos
 */
@RegisterMapper(SystemConfigDao.Mapper.class)
@UseStringTemplate3StatementLocator
public abstract class SystemConfigDao {

	/**
	 *
	 */
	static public class Mapper implements ResultSetMapper<System_Config> {

		private static final String ID = "id";
		private static final String NAME = "name";
		private static final String LABEL = "label";
		private static final String VALUE = "value";

		@Override
		public System_Config map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new System_Config(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(LABEL) ? resultSet.getString(LABEL) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from system_config <where> order by id")
	abstract public List<System_Config> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from system_config where id = :id")
	abstract public System_Config get(@Bind("id") final int id);

	/**
	 *
	 * @param name
	 * @return
	 */
	@SqlQuery("select * from system_config where name = :name")
	abstract public System_Config get(@Bind("name") final String name);

	/**
	 *
	 * @param item
	 * @param value
	 * @param clause
	 * @return
	 */
	@SqlUpdate("insert into system_config(id, name, label, value) values(system_config_seq.nextVal, :name, :label, <value>)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final System_Config item,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param item
	 * @return
	 */
	public int insert(System_Config item) {
		ClobValue value = new ClobValue(item.getValue());
		return insert(item, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param item
	 * @param value
	 * @param clause
	 */
	@SqlUpdate("update system_config set"
			+ " name = :name, "
			+ " label = :label, "
			+ " value = <value> "
			+ "where id = :id")
	abstract public void update(@BindBean final System_Config item,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param item
	 */
	public void update(System_Config item) {
		ClobValue value = new ClobValue(item.getValue());
		update(item, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from system_config where id = :id")
	abstract public int delete(@Bind("id") final int id);

}
