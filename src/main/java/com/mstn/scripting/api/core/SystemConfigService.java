/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.core;

import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.db.SystemConfigDao;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.models.System_Config;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con la configuración del sistema.
 *
 * @author amatos
 */
public abstract class SystemConfigService {

	/**
	 *
	 */
	public static final String NOT_FOUND = "No se ha encontrado el registro %s.";

	/**
	 *
	 */
	public static final String CANT_DELETE_SYSTEM_ITEM = "El registro %s es requerido por el sistema y no debe ser borrado.";

	/**
	 *
	 */
	public static final String HAS_DEPENDENTS = "El registro %s tiene dependientes en %s.";

	/**
	 *
	 */
	public static final String FORBIDDEN = "No tienes permiso para el regitro %s.";

	/**
	 *
	 */
	public static final String DATABASE_REACH_ERROR = "No se ha podido contactar con la base de datos.";

	/**
	 *
	 */
	public static final String DATABASE_CONNECTION_ERROR = "No se ha podido iniciar la conexión a base de datos.";

	/**
	 *
	 */
	public static final String DATABASE_UNEXPECTED_ERROR = "Error intentando conexion ";

	/**
	 *
	 */
	public static final String DELETE_SUCCESS = "Eliminado correctamente.";

	/**
	 *
	 */
	public static final String DELETE_UNEXPECTED_ERROR = "Un error ha ocurrido eliminando el registro %s.";

	/**
	 *
	 */
	public SystemConfigService() {

	}

	@CreateSqlObject
	abstract SystemConfigDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<System_Config> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.preparedString, where);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public System_Config get(int id) {
		System_Config item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public System_Config get(String name) {
		System_Config item = dao().get(name);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, name), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public System_Config create(System_Config item, User user) {
		int id = dao().insert(item);
		System_Config after = get(id);
		log().insert(user.getId(), "Configuraciones del sistema", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public System_Config update(System_Config item, User user) {
		System_Config before = get(item.getId());
		dao().update(item);
		System_Config after = get(item.getId());
		log().insert(user.getId(), "Configuraciones del sistema", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		System_Config before = get(id);
		int result = id == -1 ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Configuraciones del sistema", before.getId(), "Eliminar", before, null, user.getIp());
				return DELETE_SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			default:
				throw new WebApplicationException(DELETE_UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
