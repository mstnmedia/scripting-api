/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.core;

import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.db.CenterDao;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.common.EmployeeService;
import com.mstn.scripting.core.models.Center;
import com.mstn.scripting.core.models.Center_Manager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con centros.
 *
 * @author amatos
 */
public abstract class CenterService {

	private static final String NOT_FOUND = "No se ha encontrado el registro %s.";
	private static final String HAS_DEPENDENTS = "El registro %s tiene dependientes en %s.";
	private static final String FORBIDDEN = "No tienes permiso para el regitro %s.";
	private static final String DATABASE_REACH_ERROR = "No se ha podido contactar con la base de datos.";
	private static final String DATABASE_CONNECTION_ERROR = "No se ha podido iniciar la conexión a base de datos.";
	private static final String DATABASE_UNEXPECTED_ERROR = "Error intentando conexion ";
	private static final String DELETE_SUCCESS = "Eliminado correctamente.";
	private static final String DELETE_UNEXPECTED_ERROR = "Un error ha ocurrido eliminando el registro %s.";

	/**
	 *
	 */
	public CenterService() {

	}

	private EmployeeService employeeService;
	private CenterManagerService managerService;

	/**
	 *
	 * @param employeeService
	 * @return
	 */
	public CenterService setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
		return this;
	}

	/**
	 *
	 * @param centerManagerService
	 * @return
	 */
	public CenterService setCenterManagerService(CenterManagerService centerManagerService) {
		this.managerService = centerManagerService;
		return this;
	}

	@CreateSqlObject
	abstract CenterDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Center>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy) {
		WhereClause whereClause = new WhereClause(where);
		long totalRows = getCount(whereClause);
		List<Center> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Center> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return loadChildren(dao().getAll(where, pageSize, pageNumber, orderBy));
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Center> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return loadChildren(dao().getAll(where.getPreparedString(), where));
	}

	/**
	 *
	 * @param id_employee
	 * @return
	 */
	public Center getByEmployeeID(int id_employee) {
		Center item = dao().getByEmployeeID(id_employee);
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Center getOrNull(int id) {
		Center item = dao().get(id);
		if (item != null) {
			loadChildren(item);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Center get(int id) {
		Center item = getOrNull(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), HttpStatus.NOT_FOUND_404);
		}
		return item;
	}

	/**
	 *
	 * @param id_employee
	 * @return
	 */
	public Center getEmployeeCenter(int id_employee) {
		List<Integer> employeeIDs = employeeService.getEmployeeTreeUpInt(id_employee);
//		for (int i = employeeIDs.size() - 1; i >= 0; i--) {
		for (int i = 0; i < employeeIDs.size(); i++) {
			Integer employeeID = employeeIDs.get(i);
			List<Center_Manager> centerManagers = managerService.getByManagerID(employeeID);
			for (int j = 0; j < centerManagers.size(); j++) {
				Center_Manager centerManager = centerManagers.get(j);
				if (centerManager != null) {
					Center center = getOrNull(centerManager.getId_center());
					if (center != null) {
						return center;
					}
				}
			}
		}
		return null;
	}

	List<Center> loadChildren(List<Center> centers) {
		List<Integer> ids = centers.stream()
				.map(item -> item.getId())
				.collect(Collectors.toList());
		List<Center_Manager> managers = managerService.getAll(new WhereClause(
				new Where("id_center", Where.IN, ids)
		));

		for (int i = 0; i < centers.size(); i++) {
			Center c = centers.get(i);
			c.setManagers(managers.stream()
					.filter(m -> m.getId_center() == c.getId())
					.collect(Collectors.toList())
			);

		}
		return centers;
	}

	Center loadChildren(Center item) {
		item.setManagers(managerService.getAll(item.getId()));
		return item;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Center create(Center item, User user) {
		int id = dao().insert(item);
		item.setId(id);
		log().insert(user.getId(), "Centros", id, "Crear", item, item, user.getIp());
		List<Center_Manager> managers = item.getManagers();
		for (int i = 0; i < managers.size(); i++) {
			Center_Manager manager = managers.get(i);
			manager.setId_center(item.getId());
			managerService.create(manager, user);
		}
		return get(item.getId());
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Center update(Center item, User user) {
		Center before = get(item.getId());
		dao().update(item);
		log().insert(user.getId(), "Centros", before.getId(), "Actualizar", before, item, user.getIp());

		List<Center_Manager> oldManagers = managerService.getAll(item.getId());
		List<Center_Manager> newManagers = item.getManagers();

		for (int i = 0; i < oldManagers.size(); i++) {
			Center_Manager oldC = oldManagers.get(i);
			boolean found = false;
			for (int j = 0; j < newManagers.size(); j++) {
				Center_Manager newC = newManagers.get(j);
				boolean match = oldC.getId_manager() == newC.getId_manager();
				if (match) {
					oldC.setGoal_seconds(newC.getGoal_seconds());
					oldC.setMargin_percentage(newC.getMargin_percentage());
					managerService.update(oldC, user);
					newManagers.remove(j);
					found = true;
					break;
				}
			}
			if (!found) {
				managerService.delete(oldC.getId(), user);
			}
		}
		for (int j = 0; j < newManagers.size(); j++) {
			Center_Manager newC = newManagers.get(j);
			newC.setId_center(item.getId());
			managerService.create(newC, user);
		}

		return get(item.getId());
	}

	/**
	 *
	 * @param id
	 * @param notes
	 * @param user
	 * @return
	 */
	public Center updateNotes(int id, String notes, User user) {
		if (!user.isMaster() && (user.getCenterId() != id)) {
			throw new WebApplicationException(String.format(FORBIDDEN, id), HttpStatus.FORBIDDEN_403);
		}
		Center before = get(id);
		dao().updateNotes(id, notes);
		Center after = get(id);
		log().insert(user.getId(), "Centros", before.getId(), "Actualizar notas", before.getNotes(), after.getNotes(), user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(int id, User user) {
		Center before = get(id);
		List<String> dependents = new ArrayList();
		long alerts = dao().alertDependents(id);
		if (alerts > 0) {
			dependents.add("Alertas");
		}
		long categories = dao().categoryDependents(id);
		if (categories > 0) {
			dependents.add("Categorías");
		}
		long criterias = dao().criteriaDependents(id);
		if (criterias > 0) {
			dependents.add("Criterios");
		}
		long endCalls = dao().endCallDependents(id);
		if (endCalls > 0) {
			dependents.add("Razones de fines de llamadas");
		}
		long contents = dao().contentDependents(id);
		if (contents > 0) {
			dependents.add("Contenidos");
		}
		long transactions = dao().transactionDependents(id);
		if (transactions > 0) {
			dependents.add("Transacciones");
		}
		long workflows = dao().workflowDependents(id);
		if (workflows > 0) {
			dependents.add("Flujos");
		}

		int result;
		if (Utils.listNonNullOrEmpty(dependents)) {
			result = -1;
		} else {
			List<Center_Manager> managers = managerService.getAll(id);
			managers.forEach(manager -> managerService.delete(manager.getId(), user));
			result = dao().delete(id);
		}

		switch (result) {
			case 1:
				log().insert(user.getId(), "Centros", before.getId(), "Eliminar", before, null, user.getIp());
				return DELETE_SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), HttpStatus.NOT_FOUND_404);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id, String.join(", ", dependents)), HttpStatus.PRECONDITION_FAILED_412);
			default:
				throw new WebApplicationException(DELETE_UNEXPECTED_ERROR, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
