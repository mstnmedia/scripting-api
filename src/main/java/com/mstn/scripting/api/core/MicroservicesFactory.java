/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.core;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Clase que define las propiedades necesarias para especificarle a API la
 * ubicación de un microservicio.
 *
 * @author josesuero
 */
public class MicroservicesFactory {

	@NotEmpty
	private String url;

	@NotEmpty
	private String listen;

	@Min(1)
	@Max(65535)
	@NotEmpty
	private int apiport;

	@Min(1)
	@Max(65535)
	@NotEmpty
	private int adminport;

	/**
	 * @return the urls
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * *
	 *
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the apiport
	 */
	public int getApiport() {
		return apiport;
	}

	/**
	 * @param apiport the apiport to set
	 */
	public void setApiport(int apiport) {
		this.apiport = apiport;
	}

	/**
	 * @return the adminport
	 */
	public int getAdminport() {
		return adminport;
	}

	/**
	 * @param adminport the adminport to set
	 */
	public void setAdminport(int adminport) {
		this.adminport = adminport;
	}

	/**
	 * @return the listen
	 */
	public String getListen() {
		return listen;
	}

	/**
	 * @param listen the listen to set
	 */
	public void setListen(String listen) {
		this.listen = listen;
	}

}
