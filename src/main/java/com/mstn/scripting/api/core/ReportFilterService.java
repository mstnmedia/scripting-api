/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.core;

import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.db.ReportFilterDao;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.models.ReportFilter;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con filtros de reportes.
 *
 * @author amatos
 */
public abstract class ReportFilterService {
	
	private static final String NOT_FOUND = "No se ha encontrado el registro %s.";
	private static final String FORBIDDEN = "No tienes permiso para el regitro %s.";
	private static final String DATABASE_REACH_ERROR = "No se ha podido contactar con la base de datos.";
	private static final String DATABASE_CONNECTION_ERROR = "No se ha podido iniciar la conexión a base de datos.";
	private static final String DATABASE_UNEXPECTED_ERROR = "Error intentando conexion ";
	private static final String DELETE_SUCCESS = "Eliminado correctamente.";
	private static final String DELETE_UNEXPECTED_ERROR = "Un error ha ocurrido eliminando el registro %s.";
	
	public ReportFilterService() {
	}
	
	@CreateSqlObject
	abstract ReportFilterDao dao();
	
	@CreateSqlObject
	abstract public LogDao log();
	
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("(", Where.OR,
				new Where("id_employee", User.SYSTEM.getId()),
				new Where("id_employee", user.getId())
		);
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}
	
	public long getCount() {
		return dao().getCount();
	}
	
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}
	
	public Representation<List<ReportFilter>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy) {
		WhereClause whereClause = new WhereClause(where);
		long totalRows = getCount(whereClause);
		List<ReportFilter> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}
	
	public List<ReportFilter> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}
	
	public List<ReportFilter> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.getPreparedString(), where);
	}
	
	public ReportFilter getOrNull(long id) {
		ReportFilter item = dao().get(id);
		return item;
	}
	
	public ReportFilter get(long id) {
		ReportFilter item = getOrNull(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), HttpStatus.NOT_FOUND_404);
		}
		return item;
	}
	
	public ReportFilter create(ReportFilter item, User user) {
		if (item.getId_employee() != User.SYSTEM.getId()) {
			item.setId_employee(user.getId());
		}
		long id = dao().insert(item);
		item.setId(id);
		ReportFilter after = get(item.getId());
		log().insert(user.getId(), "Filtros de Reportes", id, "create", item, after, user.getIp());
		return after;
	}
	
	public ReportFilter update(ReportFilter item, User user) {
		ReportFilter before = get(item.getId());
		dao().update(item);
		log().insert(user.getId(), "Filtros de Reportes", before.getId(), "update", before, item, user.getIp());
		return get(item.getId());
	}
	
	public String delete(long id, User user) {
		ReportFilter before = get(id);
		int result = dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Filtros de Reportes", before.getId(), "delete", before, null, user.getIp());
				return DELETE_SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), HttpStatus.NOT_FOUND_404);
			default:
				throw new WebApplicationException(DELETE_UNEXPECTED_ERROR, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}
	
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}
	
	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}
	
	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}
	
}
