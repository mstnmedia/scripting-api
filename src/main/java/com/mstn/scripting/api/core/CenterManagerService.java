/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.core;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.db.CenterManagerDao;
import com.mstn.scripting.core.models.Center_Manager;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con la relación centros-empleadosS.
 *
 * @author amatos
 */
public abstract class CenterManagerService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public CenterManagerService() {
	}

	@CreateSqlObject
	abstract CenterManagerDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Center_Manager> getAll(WhereClause where) {
		where = where == null ? new WhereClause() : where;
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_center
	 * @return
	 */
	public List<Center_Manager> getAll(int id_center) {
		return dao().getAll(id_center);
	}

	/**
	 *
	 * @param id_manager
	 * @return
	 */
	public List<Center_Manager> getByManagerID(int id_manager) {
		return dao().getByManagerID(id_manager);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Center_Manager getOrNull(int id) {
		Center_Manager item = dao().get(id);
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Center_Manager get(int id) {
		Center_Manager item = getOrNull(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}

		return item;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Center_Manager create(Center_Manager item, User user) {
		int id = dao().insert(item);
		Center_Manager after = get(id);
		log().insert(user.getId(), "Supervisores de centros", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Center_Manager update(Center_Manager item, User user) {
		Center_Manager before = get(item.getId());
		dao().update(item);
		Center_Manager after = get(item.getId());
		log().insert(user.getId(), "Supervisores de centros", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		Center_Manager before = get(id);
		int result = id == -1 ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Supervisores de centros", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
