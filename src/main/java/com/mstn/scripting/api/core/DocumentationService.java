/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.core;

import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Documentation;
import com.mstn.scripting.api.db.DocumentationDao;
import com.mstn.scripting.core.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 *
 * @author josesuero
 */
public abstract class DocumentationService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String FORBIDDEN = "You don't have permissions to complete this action with item id %s.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	@CreateSqlObject
	abstract DocumentationDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where();
		if (!user.isMaster()) {
			userWheres.getGroup().add(
					new Where().setGroup(Arrays.asList(
							new Where("id_center", Where.EQUALS, user.getCenterId(), Where.OR),
							new Where("id_center", Where.EQUALS, 0, Where.OR),
							new Where("id_center", Where.IS_NULL, "", Where.OR)
					))
			);
			userWheres.getGroup().add(
					new Where().setGroup(Arrays.asList(
							new Where("id_type", Where.IN, user.getPermissions(), Where.OR),
							new Where("id_type", Where.EQUALS, "", Where.OR),
							new Where("id_type", Where.IS_NULL, "", Where.OR)
					))
			);
		}
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param after
	 * @param before
	 * @param user
	 * @return
	 */
	public boolean userHasAccessToItem(Documentation after, Documentation before, User user) {
		if (user.isMaster()) {
			return true;
		}
		if (after.getId_type() == null) {
			after.setId_type("");
		}
		if (before.getId_type() == null) {
			before.setId_type("");
		}
		String type1 = after.getId_type().trim();
		String type2 = before.getId_type().trim();
		List<String> permissions = new ArrayList(user.getPermissions());
		permissions.add("");
		if (!permissions.contains(type2)
				|| (!Utils.isNullOrEmpty(type1) && !permissions.contains(type1))) {
			throw new WebApplicationException(String.format(FORBIDDEN.replace("id", "type"), after.getId_type()), Response.Status.FORBIDDEN);
		}
		return true;
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Representation<List<Documentation>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy, User user) throws Exception {
		List<Where> clientWhere = Where.listFrom(where);
		WhereClause whereClause = getUserWhere(user, clientWhere);
		long totalRows = getCount(whereClause);
		List<Documentation> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Documentation> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param whereStr
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<Documentation> getAll(String whereStr, User user) throws Exception {
		List<Where> clientWhere = Where.listFrom(whereStr);
		WhereClause where = getUserWhere(user, clientWhere);
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public Documentation get(int id, User user) {
		Documentation item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
		}
		userHasAccessToItem(item, item, user);
		return item;
	}

//	@Transaction
	/**
	 *
	 * @param itemID
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Documentation duplicate(int itemID, User user) throws Exception {
		Documentation item = get(itemID, user);
		item.setName(item.getName() + " - Copia");
		int id = dao().insert(item);
		Documentation after = get(id, user);
		log().insert(user.getId(), "Documentación", id, "Duplicar", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Documentation create(Documentation item, User user) throws Exception {
//		if (!user.isMaster() || item.getId_center() == 0) {
//			item.setId_center(user.getCenterId());
//		}
		userHasAccessToItem(item, item, user);
		int id = dao().insert(item);
		Documentation after = get(id, user);
		log().insert(user.getId(), "Documentación", id, "Crear", item, after, user.getIp());
		return after;
	}

//	@Transaction
	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Documentation update(Documentation item, User user) throws Exception {
		Documentation before = get(item.getId(), user);
		if (!user.isMaster()) {
			item.setId_center(before.getId_center());
		}
		userHasAccessToItem(item, before, user);
		dao().update(item);
		Documentation after = get(item.getId(), user);
		log().insert(user.getId(), "Documentación", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(int id, User user) {
		Documentation before = get(id, user);
		int result = dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Documentación", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Response.Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
