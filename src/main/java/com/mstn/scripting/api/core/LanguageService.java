/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.core;

import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.db.Language;
import com.mstn.scripting.api.db.LanguageDao;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import org.eclipse.jetty.http.HttpStatus;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con idiomas por usuario.
 *
 * @author josesuero
 */
public abstract class LanguageService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	@CreateSqlObject
	abstract LanguageDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Language>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy) {
		WhereClause whereClause = new WhereClause(where);
		long totalRows = getCount(whereClause);
		List<Language> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Language> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Language> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param languageName
	 * @return
	 */
	public List<Language> getAll(String languageName) {
		return dao().getAll(languageName);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Language get(int id) {
		Language item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id_user
	 * @return
	 */
	public String getUserLanguage(int id_user) {
		Language userLang = dao().getUserLanguage(id_user);
		if (userLang == null) {
			userLang = dao().getSystemLanguage();
		}
		return userLang == null ? "es" : userLang.getName();
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	@Transaction
	public Language create(Language item, User user) {
		List<Language> userRecords = getAll(new WhereClause(new Where("id_employee", item.getId_employee())));
		if (!userRecords.isEmpty()) {
			for (int i = 0; i < userRecords.size(); i++) {
				Language userRecord = userRecords.get(i);
				if (i == 0) {
					item.setId(userRecord.getId());
					dao().update(item);
					log().insert(user.getId(), "Idiomas por usuario", userRecord.getId(), "Actualizar", userRecord, item, user.getIp());
				} else {
					dao().delete(userRecord.getId());
					log().insert(user.getId(), "Idiomas por usuario", userRecord.getId(), "Eliminar", userRecord, null, user.getIp());
				}
			}
			return item;
		}

		int id = dao().insert(item);
		Language after = get(id);
		log().insert(user.getId(), "Idiomas por usuario", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	@Transaction
	public Language update(Language item, User user) {
		Language before = get(item.getId());
		dao().update(item);
		Language after = get(item.getId());
		log().insert(user.getId(), "Idiomas por usuario", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		Language before = get(id);
		int result = id == -1 ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Idiomas por usuario", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
