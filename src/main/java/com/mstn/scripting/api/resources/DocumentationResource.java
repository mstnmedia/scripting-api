/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.api.core.DocumentationService;
import com.mstn.scripting.core.models.Documentation;
import com.mstn.scripting.core.models.IdValueParams;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a documentaciones.
 *
 * @author MSTN Media
 */
@Path("/doc")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.DOCS_LIST})
public class DocumentationResource {

	private final DocumentationService service;

	/**
	 *
	 * @param documentationService
	 */
	public DocumentationResource(DocumentationService documentationService) {
		this.service = documentationService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Documentation>> getAll(@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) throws Exception {
		Representation<List<Documentation>> result = service.getRepresentation(where, pageSize, pageNumber, orderBy, user);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Documentation>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		List<Documentation> list = service.getAll(where, user);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Documentation>> get(@Auth User user, @PathParam("id") final int id) {
		Documentation item = service.get(id, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.DOCS_ADD})
	public Representation<List<Documentation>> create(@Auth User user, @NotNull @Valid final Documentation item) throws Exception {
		Documentation inserted = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(inserted));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.DOCS_EDIT})
	public Representation<List<Documentation>> edit(@Auth User user,
			@NotNull @Valid final Documentation item, @PathParam("id") final int id) throws Exception {
		item.setId(id);
		Documentation updated = service.update(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.DOCS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 */
	@POST
	@Timed
	@Path("/getDocumentation")
	public Documentation getDocumentation(@Auth User user, @NotNull @Valid final IdValueParams params) {
		return service.get(params.getId(), user);
	}

	/**
	 *
	 * @param user
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/duplicate")
	@RolesAllowed({UserPermissions.DOCS_ADD})
	public Documentation duplicate(@Auth User user, @NotNull @Valid final IdValueParams params) throws Exception {
		return service.duplicate(params.getId(), user);
	}

}
