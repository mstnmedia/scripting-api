/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.mstn.scripting.api.ScriptingAPIApplication;
import java.io.File;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP para descarga de archivos
 * adjuntos a contenidos.
 *
 * @author MSTN Media
 */
@Path("/download")
public class DownloadResource {

	/**
	 *
	 */
	public DownloadResource() {
	}

	/**
	 *
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	@GET
	@Path("/{filename}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response download(@PathParam("filename") String fileName) throws Exception {
		String folderPath = ScriptingAPIApplication.contentsFolder;
		File file = new File(folderPath + fileName);
		if (!file.exists()) {
			return Response
					.status(HttpStatus.NOT_FOUND_404)
					.build();
		}
		String type = MediaType.APPLICATION_OCTET_STREAM;
		String contentDisposition = "attachment; filename=\"" + fileName + "\"";
		if (fileName.endsWith(".pdf")) {
			type = "application/pdf";
			contentDisposition = contentDisposition.replace("attachment; ", "");
		}
		return Response
				.ok(file)
				.type(type)
				.header("Content-Disposition", contentDisposition)
				.build();
	}

}
