/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.api.Authentication;
import com.mstn.scripting.api.ScriptingAPIApplication;
import com.mstn.scripting.api.ScriptingAPIConfiguration;
import com.mstn.scripting.api.core.CenterService;
import com.mstn.scripting.api.core.LanguageService;
import com.mstn.scripting.api.core.MicroservicesFactory;
import com.mstn.scripting.api.core.SystemConfigService;
import com.mstn.scripting.core.FileManager;
import com.mstn.scripting.core.JSON;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.Secrets;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.models.Center;
import com.mstn.scripting.core.models.ContentFile;
import com.mstn.scripting.core.models.Employee;
import com.mstn.scripting.core.models.System_Config;
import io.dropwizard.auth.Auth;
import io.dropwizard.jersey.caching.CacheControl;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.eclipse.jetty.util.URIUtil;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jose4j.lang.JoseException;
import static com.mstn.scripting.api.resources.SystemTagResource.PREFIX;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.db.common.EmployeeService;

/**
 * Clase que maneja todas las solicitudes HTTP: las recibe, las redirige al
 * microservicio correspondiente y envía la respuesta al cliente.
 *
 * Permite a los usuarios realizar login y logout. También es quien permite
 * subir los archivos por adjuntar a los contenidos.
 *
 * @author josesuero
 */
@Path("/api")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
public class ApiResource {

	/**
	 *
	 */
	public enum Method {

		/**
		 *
		 */
		GET,
		/**
		 *
		 */
		POST,
		/**
		 *
		 */
		PUT,
		/**
		 *
		 */
		DELETE
	}

	static final Logger LOGGER = Logger.getLogger(ApiResource.class.getName());
	private final Map<String, MicroservicesFactory> services;
	private final String portalURL;
	private final String appURL;
	private HttpClient httpClient;
	private EmployeeService employeeService;
	private CenterService centerService;
	private LanguageService languageService;
	private SystemConfigService configService;

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public ApiResource setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}

	/**
	 *
	 * @param employeeService
	 * @return
	 */
	public ApiResource setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
		return this;
	}

	/**
	 *
	 * @param centerService
	 * @return
	 */
	public ApiResource setCenterService(CenterService centerService) {
		this.centerService = centerService;
		return this;
	}

	/**
	 *
	 * @param languageService
	 * @return
	 */
	public ApiResource setLanguageService(LanguageService languageService) {
		this.languageService = languageService;
		return this;
	}

	/**
	 *
	 * @param configService
	 * @return
	 */
	public ApiResource setSystemConfigService(SystemConfigService configService) {
		this.configService = configService;
		return this;
	}

	/**
	 *
	 * @param services
	 * @param portalURL
	 * @param appURL
	 */
	public ApiResource(Map<String, MicroservicesFactory> services, String portalURL, String appURL) {
		this.services = services;
		this.portalURL = portalURL;
		this.appURL = appURL;
	}

	private Response makeCORS(Response.ResponseBuilder req, HttpServletRequest request) {
		String origin = Utils.coalesce(request.getHeader("Origin"), "*");
		String returnMethod = request.getHeader("Access-Control-Request-Headers");
		Response.ResponseBuilder rb = req
				//.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Origin", origin)
				.header("Access-Control-Allow-Credentials", true)
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE");
		if (!"".equals(returnMethod)) {
			rb.header("Access-Control-Allow-Headers", returnMethod);
		}

		return rb.build();
	}

	private Response createQueue(Method method, String service, String query, String payload, User user) throws Exception {
		// TODO: Why to do this loop for request listeners?
		Runnable runListeners = () -> {
			ArrayList<MicroservicesFactory> listeners;
			listeners = services.entrySet()
					.stream()
					.filter(entry -> service.startsWith(entry.getValue().getListen()))
					.map(Map.Entry::getValue)
					.collect(Collectors.toCollection(ArrayList::new));
			for (int i = 0; i < listeners.size(); i++) {
				String serviceUrl = service.substring(listeners.get(i).getUrl().length());
				requestResource(listeners.get(i).getApiport(), method, serviceUrl, query, payload, user);
			}
		};
		ArrayList<MicroservicesFactory> servers;
		servers = services.entrySet()
				.stream()
				.filter(entry -> service.startsWith(entry.getValue().getUrl()))
				.map(Map.Entry::getValue)
				.collect(Collectors.toCollection(ArrayList::new));

		if (servers.size() > 0) {
			new Thread(runListeners).start();
			String serviceUrl = service.substring(servers.get(0).getUrl().length());
			return requestResource(servers.get(0).getApiport(), method, serviceUrl, query, payload, user);
		} else {
			throw new Exception("no service available for request");
		}
	}

	private Response requestResource(int port, Method method, String service, String query, String payload, User user) {
		HttpRequestBase request = new HttpGet();
		HttpEntity entity = new BasicHttpEntity();
		try {
			StringBuilder builder = new StringBuilder("http://localhost:")
					.append(port)
					.append("/")
					.append(service);
			if (Objects.nonNull(query)) {
				builder.append("?").append(query);
			}
			String url = builder.toString();
			HttpResponse response;

			StringEntity input;
			switch (method) {
				case POST:
					input = new StringEntity(payload, "UTF-8");
					input.setContentType(MediaType.APPLICATION_JSON_UTF_8);
					request = new HttpPost(url);
					((HttpPost) request).setEntity(input);
					break;
				case PUT:
					input = new StringEntity(payload, "UTF-8");
					input.setContentType(MediaType.APPLICATION_JSON_UTF_8);
					request = new HttpPut(url);
					((HttpPut) request).setEntity(input);
					break;
				case DELETE:
					request = new HttpDelete(url);
					break;
				default:
					request = new HttpGet(url);
					break;
			}

//			request.addHeader("Authorization", bearerToken);
			request.addHeader("Cookie", Secrets.TOKEN_COOKIE_NAME + "=" + user.getToken());

			response = httpClient.execute(request);
			int statusCode = response.getStatusLine().getStatusCode();
			entity = response.getEntity();
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					entity.getContent(),
					Charset.forName("UTF-8")
			));

			StringBuilder result = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			String entityResult = result.toString();
			EntityUtils.consumeQuietly(entity);
			NewCookie cookie = updateCookie(user);
			return Response
					.status(statusCode)
					.cookie(cookie)
					.entity(entityResult)
					.build();
		} catch (Exception e) {
			return getErrorResponse(e);
		} finally {
			EntityUtils.consumeQuietly(entity);
			request.releaseConnection();
		}
	}

	/**
	 *
	 * @param request
	 * @return
	 */
	@OPTIONS
	@Path("/userInfo")
	public Response userInfo(@Context HttpServletRequest request) {
		return corsMyResource(request);
	}

	/**
	 *
	 * @param user
	 * @return
	 */
	@GET
	@Timed
	@Path("/userInfo")
	public Response userInfo(@Auth User user) {
		try {
			NewCookie cookie = updateCookie(user);
			return Response
					.status(200)
					.cookie(cookie)
					.entity(user)
					.build();
		} catch (Exception ex) {
			return getErrorResponse(ex);
		}
	}

	private NewCookie updateCookie(User user) throws Exception {
		ScriptingAPIConfiguration config = ScriptingAPIConfiguration.current;
		int maxAge = config.getTokenTime() * 60;
		long expired = DATE.nowAsLong() + (maxAge * 1000);
		User newUser = new User(
				user.getId(), user.getName(),
				user.getRole(), user.getPermissions(),
				user.getEmployee(), user.getCenter(),
				user.getLanguage(), user.getIp(),
				user.getLogged(), expired
		);
		String token = newUser.generateToken();
		String hostname = config.getCookiePath();
		NewCookie cookie = new NewCookie(
				Secrets.TOKEN_COOKIE_NAME,
				token, "/", hostname,
				"Authentication token",
				maxAge, false
		);
		return cookie;
	}

	/**
	 *
	 * @param app
	 * @param usr
	 * @param pawToken
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@GET
	@Path("/login")
	@CacheControl(noCache = true, noStore = true, mustRevalidate = true, maxAge = 0)
	public final Response login(
			@QueryParam("app") String app,
			@QueryParam("usr") String usr,
			@QueryParam("token") long pawToken,
			@Context HttpServletRequest request
	) throws Exception {
		User user = null;
		String ip = request.getRemoteAddr();
		try {
			boolean debugMode = ScriptingAPIApplication.debugMode;
			boolean isValidPAWToken = debugMode || Authentication.isValidPAWToken(app, usr, pawToken);
			if (!isValidPAWToken) {
				employeeService.log().insert(0, "Bitácora de acceso", pawToken, "Error en inicio de sessión",
						String.format("El token PAW %s no es válido para el usuario %s en la aplicación %s", pawToken, usr, app),
						user, ip);
				return logout(null, request);
			}

			int id_employee;
			if (debugMode && ScriptingAPIApplication.debugUser > 0) {
				id_employee = ScriptingAPIApplication.debugUser;
			} else {
				id_employee = Authentication.getUserIDFromPAW(usr);
			}
			Employee employee = employeeService.getOrNull(id_employee);
			baseLog(Level.CONFIG, "Employee {0}: {1}", new Object[]{id_employee, JSON.toString(employee)});
			if (employee == null) {
				employeeService.log().insert(id_employee, "Bitácora de acceso", pawToken, "Error en inicio de sessión",
						String.format("El empleado %s con tarjeta %s no se encuentra en la base de datos.", usr, id_employee),
						user, ip);
				return logout(null, request);
			}

			Authentication.PAW_Role pawRole;
			if (debugMode && ScriptingAPIApplication.debugRole > 0) {
				pawRole = new Authentication.PAW_Role(ScriptingAPIApplication.debugRole, usr);
			} else {
				pawRole = Authentication.pawBuscaRol(app, usr);
			}
			baseLog(Level.CONFIG, "PAW Role: {0}", JSON.toString(pawRole));
			String role = pawRole.description;
			List<String> permissions = Authentication.pawBuscaRolAccesos(app, pawRole.id);
			if (permissions.isEmpty() && debugMode) {
				baseLog(Level.CONFIG, "Not permissions. Using MASTER role.");
				permissions = UserPermissions.getAllPermissions();
			}

			Center center = centerService.getEmployeeCenter(employee.getId());
			String language = languageService.getUserLanguage(employee.getId());

			int tokenTime = ScriptingAPIApplication.tokenTime;
			Date logged = new Date();
			long expired = DATE.nowAsLong() + (tokenTime * 60 * 1000);

			user = new User(
					id_employee, employee.getName(),
					role, permissions,
					employee, center, language,
					ip, logged, expired
			);
			String token = user.generateToken();

			baseLog(Level.CONFIG, "Generated token: {0}", token);
			if (token.length() > 4096) {
				String warning = "El token tiene más de 4096 carácteres. El navegador lo podrá ignorar.";
				LOGGER.log(Level.WARNING, warning);
				employeeService.log().insert(user.getId(), "Bitácora de acceso", pawToken, "Advertencia de inicio de sesión", warning, user, ip);
			}
			URI uri = UriBuilder.fromUri(appURL).build();
			String hostname = request.getServerName();
			int maxAge = tokenTime * 60;
			NewCookie cookie = new NewCookie(
					Secrets.TOKEN_COOKIE_NAME,
					token,
					"/",
					hostname,
					"Authentication token",
					maxAge,
					false
			);
			employeeService.log().insert(user.getId(), "Bitácora de acceso", pawToken, "Inicio de sesión", user.getRole(), user, ip);
			return Response.seeOther(uri)
					.cookie(cookie)
					.build();
		} catch (Exception ex) {
			long userID = user == null ? 0 : user.getId();
			employeeService.log().insert(userID, "Bitácora de acceso", pawToken, "Error en inicio de sesión", ex.getMessage(), ex, ip);
			return getErrorResponse(ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param request
	 * @return
	 * @throws JoseException
	 */
	@GET
	@Path("/logout")
	public final Response logout(@Auth User user, @Context HttpServletRequest request) throws JoseException {
		URI uri = UriBuilder.fromUri(portalURL).build();
		String hostname = request.getServerName();
		NewCookie cookie = new NewCookie(
				Secrets.TOKEN_COOKIE_NAME, "",
				"/", hostname,
				"Authentication token",
				0, false
		);
		try {
			int userID = 0;
			String userRole = "";
			String ip = request.getRemoteAddr();
			if (user != null) {
				userID = user.getId();
				userRole = user.getRole();
			}
			employeeService.log().insert(userID, "Bitácora de acceso", userID, "Cierre de sesión", userRole, user, ip);
			return Response
					.seeOther(uri)
					.cookie(cookie)
					.build();
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			return Response
					.status(500)
					.cookie(cookie)
					.entity(ex.getMessage())
					.build();
		}
	}

	/**
	 *
	 * @param request
	 * @return
	 */
	@OPTIONS
	@Path("/uploadContent")
	public Response uploadContent(@Context HttpServletRequest request) {
		return corsMyResource(request);
	}

	/**
	 *
	 * @param user
	 * @param inputStream
	 * @param fileDetail
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("/uploadContent")
	public Response uploadContent(
			@Auth User user,
			@FormDataParam("file") InputStream inputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail
	) throws Exception {
		try {
			String folderPath = ScriptingAPIApplication.uploadFolder;
			long now = DATE.nowAsLong();
			String fileName = now + "_" + fileDetail.getFileName().replace(" ", "");
			String filePath = folderPath + fileName;
			LOGGER.log(Level.INFO, "File will be uploaded to: {0}", filePath);

			FileManager.saveFile(inputStream, filePath);
			NewCookie cookie = updateCookie(user);
			return Response
					.ok(new ContentFile(fileDetail.getFileName(), fileName, now))
					.cookie(cookie)
					.build();
		} catch (Exception ex) {
			return getErrorResponse(ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param code
	 * @return
	 */
	@GET
	@Timed
	@Path("/systemTag/{code}")
	public Response systemTag(@Auth User user, @PathParam("code") String code) {
		try {
			NewCookie cookie = updateCookie(user);
			System_Config item = configService.get(PREFIX + code);
			return Response
					.ok(item)
					.cookie(cookie)
					.build();
		} catch (Exception ex) {
			return getErrorResponse(ex);
		}
	}

	// This OPTIONS request/response is necessary
	// if you consumes other format than text/plain or
	// if you use other HTTP verbs than GET and POST
	/**
	 *
	 * @param request
	 * @return
	 */
	@OPTIONS
	@Path("/{subResources:.*}")
	public Response corsMyResource(@Context HttpServletRequest request) {
		return makeCORS(Response.ok(), request);
	}

	/**
	 *
	 * @param user
	 * @param subResources
	 * @param uriInfo
	 * @return
	 */
	@GET
	@Timed
	@Path("/{subResources:.*}")
	public Response get(@Auth User user, @PathParam("subResources") String subResources, @Context UriInfo uriInfo) {
		try {
			String query = uriInfo.getRequestUri().getQuery();
			query = URIUtil.encodePath(query);
			return createQueue(Method.GET, new StringBuilder("/").append(subResources).toString(), query, null, user);
		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	/**
	 *
	 * @param user
	 * @param subResources
	 * @param uriInfo
	 * @param payload
	 * @return
	 */
	@POST
	@Timed
	@Path("/{subResources:.*}")
	@Consumes(MediaType.APPLICATION_JSON_UTF_8)
	public Response post(@Auth User user, @PathParam("subResources") String subResources, @Context UriInfo uriInfo, String payload) {
		try {
			String query = uriInfo.getRequestUri().getQuery();
			return createQueue(Method.POST, new StringBuilder("/").append(subResources).toString(), query, payload, user);
		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	/**
	 *
	 * @param user
	 * @param subResources
	 * @param uriInfo
	 * @param payload
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{subResources:.*}")
	@Consumes(MediaType.APPLICATION_JSON_UTF_8)
	public Response put(@Auth User user, @PathParam("subResources") String subResources, @Context UriInfo uriInfo, String payload) {
		try {
			String query = uriInfo.getRequestUri().getQuery();
			return createQueue(Method.PUT, new StringBuilder("/").append(subResources).toString(), query, payload, user);
		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	/**
	 *
	 * @param user
	 * @param subResources
	 * @param uriInfo
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{subResources:.*}")
	public Response delete(@Auth User user, @PathParam("subResources") String subResources, @Context UriInfo uriInfo) {
		try {
			String query = uriInfo.getRequestUri().getQuery();
			query = URIUtil.encodePath(query);
			return createQueue(Method.DELETE, new StringBuilder("/").append(subResources).toString(), query, null, user);
		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	/**
	 *
	 * @param ex
	 * @return
	 */
	public Response getErrorResponse(Exception ex) {
		Utils.logException(ApiResource.class, ex.getMessage(), ex);
		return Response
				.status(500)
				.entity(ex.getMessage())
				.build();
	}

	void baseLog(Level level, String msg, Object... params) {
		LOGGER.log(level, msg, params);
	}
}
