/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.core.ReportFilterService;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.Log;
import com.mstn.scripting.core.models.ReportFilter;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a los filtros de
 * reportes.
 *
 * @author amatos
 */
@Path("/reportfilter")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
@RolesAllowed({UserPermissions.REPORTS_SHOW})
public class ReportFilterResource {

	private final ReportFilterService service;

	public ReportFilterResource(ReportFilterService service) {
		this.service = service;
	}

	@GET
	@Timed
	public Representation<List<ReportFilter>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		List<Where> clientWhere = Where.listFrom(where);
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		List<ReportFilter> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	@GET
	@Timed
	@Path("/getPage")
	public Representation<List<ReportFilter>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue("name") @QueryParam("orderBy") String orderBy
	) {
		Representation<List<ReportFilter>> result = service.getRepresentation(where, pageSize, pageNumber, orderBy);
		return result;
	}

	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<ReportFilter>> get(@Auth User user, @PathParam("id") final long id) {
		ReportFilter item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	@POST
	@Timed
	@RolesAllowed({UserPermissions.REPORTS_SHOW})
	public Representation<List<ReportFilter>> create(@Auth User user, @NotNull @Valid final ReportFilter item) {
		ReportFilter created = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
	}

	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.REPORTS_SHOW})
	public Representation<List<ReportFilter>> edit(
			@Auth User user,
			@PathParam("id") final long id,
			@NotNull @Valid final ReportFilter item) {
		item.setId(id);
		ReportFilter updated = service.update(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
	}

	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.REPORTS_SHOW})
	public Representation<String> delete(@Auth User user, @PathParam("id") final long id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
	}

	@POST
	@Timed
	@Path("/Log_Generation")
	@RolesAllowed({UserPermissions.REPORTS_SHOW})
	public long create(@Auth User user, @NotNull final Log item, @Context HttpServletRequest request) {
		Log log = new Log(0, user.getId(), 
				item.getTable_name(),item.getId_record(),
				item.getAction(), new Date(),
				item.getBefore_value(), item.getAfter_value(),
				request.getRemoteAddr()
		);
		long createdID = service.log().insert(log);
		return createdID;
	}
}
