/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.core.CenterManagerService;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.models.Center_Manager;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a la relación centro-empleados.
 *
 * @author MSTN Media
 */
@Path("/centerManager")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
@RolesAllowed({UserPermissions.CENTERS_LIST})
public class CenterManagerResource {

	private final CenterManagerService service;

	/**
	 *
	 * @param service
	 */
	public CenterManagerResource(CenterManagerService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Center_Manager>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		WhereClause whereClause = new WhereClause(where);
		List<Center_Manager> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Center_Manager>> get(@Auth User user, @PathParam("id") final int id) {
		Center_Manager item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.CENTERS_ADD})
	public Representation<List<Center_Manager>> create(@Auth User user, @NotNull @Valid final Center_Manager item) {
		Center_Manager created = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param item
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CENTERS_EDIT})
	public Representation<List<Center_Manager>> edit(
			@Auth User user,
			@PathParam("id") final int id,
			@NotNull @Valid final Center_Manager item) {
		item.setId(id);
		Center_Manager updated = service.update(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.CENTERS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
	}

}
