/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.api.core.LanguageService;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.core.SystemConfigService;
import com.mstn.scripting.api.db.Language;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.models.System_Config;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a configuración del
 * sistema.
 *
 * @author MSTN Media
 */
@Path("/systemTag")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
public class SystemTagResource {

	static final String PREFIX = "language_";
	private final SystemConfigService service;
	private final LanguageService languageService;

	/**
	 *
	 * @param service
	 * @param languageService
	 */
	public SystemTagResource(SystemConfigService service, LanguageService languageService) {
		this.service = service;
		this.languageService = languageService;
	}

	/**
	 *
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@RolesAllowed({UserPermissions.LANGUAGE_LIST})
	public Representation<List<System_Config>> getAll(@Auth User user) throws Exception {
		WhereClause whereClause = new WhereClause(new Where("name", "like", PREFIX));
		List<System_Config> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.LANGUAGE_LIST})
	public Representation<List<System_Config>> get(@Auth User user, @PathParam("id") final int id) {
		System_Config item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.LANGUAGE_ADD})
	public Representation<List<System_Config>> create(@Auth User user, @NotNull @Valid final System_Config item) {
		List<System_Config> list = new ArrayList<>();
		list.add(service.create(item, user));
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.LANGUAGE_EDIT})
	public Representation<List<System_Config>> edit(
			@Auth User user,
			@NotNull @Valid final System_Config item,
			@PathParam("id") final int id) {
		item.setId(id);

		List<System_Config> list = new ArrayList<>();
		list.add(service.update(item, user));
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.LANGUAGE_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		System_Config systemTag = service.get(id);
		if (systemTag != null && systemTag.getName().startsWith(PREFIX)) {
			List<Language> dependents = languageService.getAll(systemTag.getName());
			if (dependents.size() > 0) {
				throw new WebApplicationException(String.format(SystemConfigService.HAS_DEPENDENTS, id, "Idiomas por usuario"), HttpStatus.PRECONDITION_FAILED_412);
			}
			if (id < 0) {
				throw new WebApplicationException(String.format(SystemConfigService.CANT_DELETE_SYSTEM_ITEM, id), HttpStatus.PRECONDITION_FAILED_412);
			}
			String deleted = service.delete(id, user);
			return new Representation<>(HttpStatus.OK_200, deleted);
		}
		throw new WebApplicationException(String.format(SystemConfigService.NOT_FOUND, id), HttpStatus.NOT_FOUND_404);
	}

}
