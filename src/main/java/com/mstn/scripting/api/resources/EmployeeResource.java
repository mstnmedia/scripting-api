/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.common.EmployeeService;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.Employee;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP de consulta de empleados.
 *
 * @author MSTN Media
 */
@Path("/employee")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
@RolesAllowed({UserPermissions.EMPLOYEES_LIST})
public class EmployeeResource {

	private final EmployeeService service;

	/**
	 *
	 * @param service
	 */
	public EmployeeResource(EmployeeService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Employee>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue("name") @QueryParam("orderBy") String orderBy
	) {
		Representation<List<Employee>> result = service.getRepresentation(where, pageSize, pageNumber, orderBy);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Employee>> getAll(@Auth User user, @QueryParam("where") String where) {
		WhereClause whereClause = new WhereClause(where);
		List<Employee> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Employee>> get(@Auth User user, @PathParam("id") final int id) {
		List<Employee> list = new ArrayList<>();
		list.add(service.get(id));
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/employeeTreeUp/{id}")
	public Representation<List<Employee>> getEmployeeTreeUp(@Auth User user, @PathParam("id") final int id) {
		List<Employee> list = service.getEmployeeTreeUp(id);
		return new Representation<>(HttpStatus.OK_200, list);
	}

//	@POST
//	@Timed
//	public Representation<List<Employee>> create(@Auth User user, @NotNull @Valid final Employee item) {
//		List<Employee> list = new ArrayList<>();
//		list.add(service.create(item));
//		return new Representation<>(HttpStatus.OK_200, list);
//	}
//	@PUT
//	@Timed
//	@Path("/{id}")
//	public Representation<List<Employee>> edit(@Auth User user, @NotNull @Valid final Employee item,
//			@PathParam("id") final int id) {
//		item.setId(id);
//
//		List<Employee> list = new ArrayList<>();
//		list.add(service.update(item));
//		return new Representation<>(HttpStatus.OK_200, list);
//	}
//	@DELETE
//	@Timed
//	@Path("/{id}")
//	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
//		return new Representation<>(HttpStatus.OK_200, service.delete(id));
//	}
}
