/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.api.core.LanguageService;
import com.mstn.scripting.api.db.Language;
import com.mstn.scripting.core.enums.Pagination;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a la configuración
 * de idiomas por usuario.
 *
 * @author MSTN Media
 */
@Path("/language")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
@RolesAllowed({UserPermissions.LANGUAGE_LIST})
public class LanguageResource {

	private final LanguageService service;

	/**
	 *
	 * @param languageService
	 */
	public LanguageResource(LanguageService languageService) {
		this.service = languageService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@GET
	@Timed
	public Representation<List<Language>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue("main, id") @QueryParam("orderBy") String orderBy
	) {
		Representation<List<Language>> result = service.getRepresentation(where, pageSize, pageNumber, orderBy);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Language>> getAll(@Auth User user, @QueryParam("where") String where) {
		WhereClause whereClause = new WhereClause(where);
		List<Language> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Language>> get(
			@Auth User user,
			@PathParam("id") final int id) {
		Language item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.LANGUAGE_ADD})
	public Representation<List<Language>> create(
			@Auth User user,
			@NotNull @Valid final Language item) {
		Language created = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param item
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.LANGUAGE_EDIT})
	public Representation<List<Language>> edit(
			@Auth User user,
			@PathParam("id") final int id,
			@NotNull @Valid final Language item) {
		item.setId(id);
		Language updated = service.update(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.LANGUAGE_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
	}

	/**
	 *
	 * @param user
	 * @return
	 */
	@POST
	@Timed
	@Path("/userLanguage")
	public String getUserLanguage(@Auth User user) {
		return user.getLanguage();
	}
}
