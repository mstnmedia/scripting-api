/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api.health;

import com.codahale.metrics.health.HealthCheck;

/**
 * Clase que se registra para consultar el estado del microservicio.
 *
 * @author amatos
 */
public class ScriptingAPIHealthCheck extends HealthCheck {

	private static final String HEALTHY = "The API Service is healthy for read and write";
	private static final String UNHEALTHY = "The API Service is not healthy. ";
	private static final String MESSAGE_PLACEHOLDER = "{}";

	/**
	 *
	 */
	public ScriptingAPIHealthCheck() {

	}

	@Override
	protected Result check() throws Exception {
		String dbHealthStatus = null; //transactionService.performHealthCheck();

		if (dbHealthStatus == null) {
			return Result.healthy(HEALTHY);
		} else {
			return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, dbHealthStatus);
		}
	}
}
