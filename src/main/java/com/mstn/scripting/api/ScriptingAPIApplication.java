package com.mstn.scripting.api;

import com.mstn.scripting.api.core.LanguageService;
import com.mstn.scripting.api.core.MicroservicesFactory;
import com.mstn.scripting.api.health.ScriptingAPIHealthCheck;
import com.mstn.scripting.api.resources.ApiResource;
import com.mstn.scripting.api.resources.LanguageResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.PolymorphicAuthDynamicFeature;
import io.dropwizard.auth.PolymorphicAuthValueFactoryProvider;
import io.dropwizard.auth.PrincipalImpl;
import io.dropwizard.auth.basic.BasicCredentials;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.jose4j.jwt.consumer.JwtContext;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.mstn.scripting.api.core.CenterManagerService;
import com.mstn.scripting.api.core.CenterService;
import com.mstn.scripting.api.core.DocumentationService;
import com.mstn.scripting.api.core.ReportFilterService;
import com.mstn.scripting.api.core.SystemConfigService;
import com.mstn.scripting.api.resources.CenterManagerResource;
import com.mstn.scripting.api.resources.CenterResource;
import com.mstn.scripting.api.resources.DelegationResource;
import com.mstn.scripting.api.resources.DocumentationResource;
import com.mstn.scripting.api.resources.DownloadResource;
import com.mstn.scripting.api.resources.EmployeeResource;
import com.mstn.scripting.api.resources.ReportFilterResource;
import com.mstn.scripting.api.resources.SystemTagResource;
import com.mstn.scripting.core.auth.AuthFilterUtils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.common.DelegationService;
import com.mstn.scripting.core.db.common.EmployeeService;
import io.dropwizard.client.HttpClientBuilder;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.bundles.DBIExceptionsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import org.apache.http.client.HttpClient;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.skife.jdbi.v2.DBI;

/**
 * Clase ejecutable que inicia el servidor y registra los componentes necesarios
 * para el funcionamiento del microservicio API.
 *
 * @author MSTN Media
 */
public class ScriptingAPIApplication extends Application<ScriptingAPIConfiguration> {

	/**
	 *
	 */
	static public URL WSDL_PAW;

	/**
	 *
	 */
	static public String uploadFolder;

	/**
	 *
	 */
	static public String contentsFolder;

	/**
	 *
	 */
	static public boolean debugMode;

	/**
	 *
	 */
	static public int debugRole;

	/**
	 *
	 */
	static public int debugUser;

	/**
	 *
	 */
	static public int tokenTime;

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		new ScriptingAPIApplication().run(args);
	}

	@Override
	public String getName() {
		return "ScriptingAPI";
	}

	@Override
	public void initialize(final Bootstrap<ScriptingAPIConfiguration> bootstrap) {
		bootstrap.addBundle(new DBIExceptionsBundle());
	}

	@Override
	public void run(final ScriptingAPIConfiguration config, final Environment env) throws MalformedURLException {
		ScriptingAPIConfiguration.current = config;
		WSDL_PAW = new URL(config.getWsdlURL());
		debugMode = config.getDebugMode();
		debugRole = config.getDebugRole();
		debugUser = config.getDebugUser();
		tokenTime = config.getTokenTime();
		uploadFolder = config.getUploadFolder();
		contentsFolder = config.getContentsFolder();
		TimeZone.setDefault(TimeZone.getTimeZone(config.getTimeZone()));
		Locale.setDefault(Locale.forLanguageTag(config.getLocale()));
		registerResources(config, env);
		registerAuthFilters(env);
	}

	private void registerResources(final ScriptingAPIConfiguration config, final Environment env) {
		final DBIFactory factory = new DBIFactory();
		final DBI dbi = factory.build(env, config.getDataSourceFactory(), "database");

		final FilterRegistration.Dynamic cors = env.servlets().addFilter("CORS", CrossOriginFilter.class);
		cors.setInitParameter("allowedOrigins", "*");
		cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
		cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

		// Add URL mapping
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

		final Map<String, MicroservicesFactory> services = config.getMicroservices();

		final HttpClient httpClient = new HttpClientBuilder(env)
				.using(config.getHttpClientConfiguration())
				.build(getName());
		final String portalURL = config.getPortalURL();
		final String appURL = config.getAppURL();

		final DocumentationService documentationService = dbi.onDemand(DocumentationService.class);
		final DelegationService delegationService = dbi.onDemand(DelegationService.class);
		final EmployeeService employeeService = dbi.onDemand(EmployeeService.class)
				.setDelegationService(delegationService);
		final CenterManagerService centerManagerService = dbi.onDemand(CenterManagerService.class);
		final CenterService centerService = dbi.onDemand(CenterService.class)
				.setEmployeeService(employeeService)
				.setCenterManagerService(centerManagerService);
		final LanguageService languageService = dbi.onDemand(LanguageService.class);
		final SystemConfigService configService = dbi.onDemand(SystemConfigService.class);
		final ReportFilterService reportFilterService = dbi.onDemand(ReportFilterService.class);

		ScriptingAPIHealthCheck healthCheck = new ScriptingAPIHealthCheck();
		env.healthChecks().register("API_Health", healthCheck);
		env.jersey().register(new ApiResource(services, portalURL, appURL)
				.setHttpClient(httpClient)
				.setEmployeeService(employeeService)
				.setCenterService(centerService)
				.setLanguageService(languageService)
				.setSystemConfigService(configService)
		);

		env.jersey().register(MultiPartFeature.class);
		env.jersey().register(new CenterResource(centerService));
		env.jersey().register(new CenterManagerResource(centerManagerService));
		env.jersey().register(new DownloadResource());
		env.jersey().register(new DelegationResource(delegationService));
		env.jersey().register(new DocumentationResource(documentationService));
		env.jersey().register(new EmployeeResource(employeeService));
		env.jersey().register(new LanguageResource(languageService));
		env.jersey().register(new SystemTagResource(configService, languageService));
		env.jersey().register(new ReportFilterResource(reportFilterService));
	}

	/**
	 * Registers the filters that will handle authentication
	 */
	private void registerAuthFilters(Environment environment) {
		AuthFilterUtils authFilterUtils = new AuthFilterUtils();
		final AuthFilter<BasicCredentials, PrincipalImpl> basicFilter = authFilterUtils.buildBasicAuthFilter();
		final AuthFilter<JwtContext, User> jwtFilter = authFilterUtils.buildJwtAuthFilter();

		final PolymorphicAuthDynamicFeature feature = new PolymorphicAuthDynamicFeature<>(
				ImmutableMap.of(PrincipalImpl.class, basicFilter, User.class, jwtFilter));
		final AbstractBinder binder = new PolymorphicAuthValueFactoryProvider.Binder<>(
				ImmutableSet.of(PrincipalImpl.class, User.class));

		environment.jersey().register(feature);
		environment.jersey().register(binder);
		environment.jersey().register(RolesAllowedDynamicFeature.class);
	}
}
