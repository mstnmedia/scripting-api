/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.api;

import com.mstn.scripting.core.Utils;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import org.tempuri.ArrayOfString;
import org.tempuri.Service1;
import org.tempuri.Service1Soap;

/**
 * Clase para interactuar con el web service del Portal de Aplicaciones Web
 * (PAW).
 *
 * @author amatos
 */
public class Authentication {

	static final Logger LOGGER = Logger.getLogger(Authentication.class.getName());
	static Service1Soap SOAP = null;

	/**
	 *
	 * @return
	 */
	static public Service1Soap getSoap() {
		if (SOAP == null) {
			URL wsdlLocation = ScriptingAPIApplication.WSDL_PAW;
			QName qName = (new Service1()).getServiceName();
			Service1 service = new Service1(wsdlLocation, qName);
			SOAP = service.getService1Soap();
		}
		return SOAP;
	}

	/**
	 *
	 * @param usr
	 * @return
	 * @throws Exception
	 */
	public static List<String> pawGetUserInf(String usr) throws Exception {
		try {
			ArrayOfString objResponse = getSoap().getUserInf(usr);
			List<String> response = objResponse.getString();
			baseLog(Level.INFO, "PAW UserInf: {0}", String.join(",", response));
			return response;
		} catch (Exception ex) {
			throw new Exception("Error consultando PAW: " + ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param app
	 * @param usr
	 * @return
	 */
	public static PAW_Role pawBuscaRol(String app, String usr) {
		int id = getSoap().buscaRol(app, usr);
		String description = getSoap().buscaRolDescripcion(app, usr);
		baseLog(Level.INFO, "PAW User Rol: {0} {1}", new Object[]{id, description});
		return new PAW_Role(id, description);
	}

	/**
	 *
	 * @param app
	 * @param rolId
	 * @return
	 */
	public static List<String> pawBuscaRolAccesos(String app, int rolId) {
		List<String> response = getSoap().buscaAccesos(app, rolId).getString();
		baseLog(Level.INFO, "PAW User Permissions: {0}", response);
		return response;
	}

	/**
	 *
	 * @param app
	 * @param usr
	 * @param token
	 * @return
	 */
	public static boolean isValidPAWToken(String app, String usr, long token) {
		boolean response = getSoap().autenticaToken(usr, app, token);
		baseLog(Level.INFO, "PAW valid token {0}: {1}", new Object[]{token, response});
		return response;
	}

	/**
	 *
	 * @param usr
	 * @return
	 * @throws Exception
	 */
	public static int getUserIDFromPAW(String usr) throws Exception {
		List<String> userProps = pawGetUserInf(usr);
		String userID = userProps.get(1);
		if (Utils.stringIsNullOrEmpty(userID)) {
			throw new Exception("Usuario no registrado en PAW");
		}
		int tarjeta = Integer.parseInt(userID);
		return tarjeta;
	}

	static void baseLog(Level level, String msg, Object... params) {
		if (ScriptingAPIApplication.debugMode) {
			LOGGER.log(level, msg, params);
		}
	}

	/**
	 *
	 */
	static public class PAW_Role {

		/**
		 *
		 */
		public int id;

		/**
		 *
		 */
		public String description;

		/**
		 *
		 * @param id
		 * @param description
		 */
		public PAW_Role(int id, String description) {
			this.id = id;
			this.description = description;
		}

	}

}
